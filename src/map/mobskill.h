﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CMOBSKILL_H
#define _CMOBSKILL_H

#include "../common/cbasetypes.h"
#include "../common/mmo.h"

#define MAX_MOBSKILL_ID	4096

// TODO: implement this
enum SKILLFLAG
{
	SKILLFLAG_NONE           = 0x000,
	SKILLFLAG_WS             = 0x001, // Weaponskill
	SKILLFLAG_TWO_HOUR       = 0x002, // 2 Hour Ability
    SKILLFLAG_SPECIAL        = 0x004, // Special Attack
    SKILLFLAG_HIT_ALL        = 0x008, // Attack that hits everyone
    SKILLFLAG_REPLACE_ATTACK = 0x010, // Replacement for a regular attack
    SKILLFLAG_DRAW_IN        = 0x020  // Draw in effect
};

class CMobSkill
{
public:

	CMobSkill(uint16 id);

    bool        hasMissMsg(); // Has a message for a miss
    bool        isAoE();      // Is the skill AoE
    bool        isConal();    // Is the skill Conal
    bool        isSingle();   // Is the skill Single target
    bool        isTwoHour();  // Is the skill a 2 Hour

	uint16		getID();             // Return the skill's ID
	uint16		getAnimationID();    // Return the skill's Animation
	uint16		getfamilyID();       // Return the skill's Family ID
	uint8		getAoe();            // Return the AoE type
	float		getDistance();       // Return the distance range
	uint8		getFlag();           // Return the type flag
    uint16      getAnimationTime();  // Return the animation time
    uint16      getActivationTime(); // Return the activation time
	uint16		getMsg();            // Return the skill's message
    uint16      getAoEMsg();         // Return the skill's AoE message
	uint16		getValidTargets();   // Return valid targets of the skill
	int16       getTP();             // Return TP after use
    uint16      getTotalTargets();   // Return total targets of the skill
    uint16      getMsgForAction();   // Return the action's message
    float       getRadius();         // Return the radius of the skill's range
    int16		getParam();          // Return the parameters of the skill
    uint8       getKnockback();      // Return the knockback amount of the skill
    uint8       getSkillchain();     // Return the skillchain for the skill

    bool        isDamageMsg();       // Is the message for damage

	void		setID(uint16 id);                         // Set the skill's ID
	void		setAnimationID(uint16 aid);               // Set the skill's Animation ID
	void		setfamilyID(uint16 familyID);             // Set the skill's Family ID
	void		setAoe(uint8 aoe);                        // Set the skill's AoE type
	void		setDistance(float distance);              // Set the skill's range
	void		setFlag(uint8 flag);                      // Set the skill's flag
    void        setAnimationTime(uint16 AnimationTime);   // Set the skill's animation time
    void        setActivationTime(uint16 ActivationTime); // Set the skill's activation time
	void		setMsg(uint16 msg);                       // Set the skill's message
	void		setValidTargets(uint16 targ);             // Set the Valid Targets of the skill
	void		setTP(int16 tp);                          // Set the TP after the skill
	void		setTotalTargets(uint16 targets);          // Set the total number of targets
	void		setParam(int16 value);                    // Set the skill's parameters
	void		setKnockback(uint8 knockback);            // Set the skill's knockback value
	void		setSkillchain(uint8 skillchain);          // Set the skill's skillchain

	const int8* getName();                                // Return the skill's name
	void		setName(int8* name);                      // Set the skill's name

private:

	uint16		m_ID;               // ID of skill
    uint16      m_TotalTargets;     // Total targets of skill
	uint16      m_FamilyID;         // Family ID for Mobskill
	int16		m_Param;            // Skill parameters
	uint16		m_AnimID;			// Animation id
	uint8       m_Aoe;              // не используется AoE type
	float		m_Distance;         // не используется Distance range
	uint8		m_Flag;             // не используется Type Flag
    uint8       m_ValidTarget;		// Target
    uint16      m_AnimationTime;	// How long the TP animation lasts for in ms
    uint16      m_ActivationTime;	// How long the mob prepares the TP move for
	uint16		m_Message;			// Message param, scripters can edit this depending on self/resist/etc.
	int16		m_TP;				// The TP at the time of finish readying (for scripts)
    uint8       m_knockback;        // Knockback value (0-7)
    uint8       m_skillchain;       // Weaponskill ID of skillchain properties

	string_t	m_name;             // Name of skill
};

#endif
