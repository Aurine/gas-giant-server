﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CPARTY_H
#define _CPARTY_H

#include "../common/cbasetypes.h"

#include <vector>

class CBasicPacket;
class CBattleEntity;
class CCharEntity;
class CAlliance;

enum PARTYTYPE
{
    PARTY_PCS,
    PARTY_MOBS,
};

enum PARTYFLAG
{
	PARTY_SECOND	= 0x0001,
	PARTY_THIRD		= 0x0002,
	PARTY_LEADER	= 0x0004,
	ALLIANCE_LEADER = 0x0008,
	PARTY_QM		= 0x0010,
	PARTY_SYNC		= 0x0100
};

/************************************************************************
*																		*
*  Класс группы	персонажей												*
*  Class group of characters											*
*																		*
************************************************************************/

class CParty 
{
public:

    CParty(CBattleEntity* PEntity);
	
    uint32 GetPartyID();                                // узнаем уникальный ID группы Find out the unique ID of
    uint16 GetMemberFlags(CBattleEntity* PEntity);      // получаем список флагов персонажа Get a list of character flags
    uint8  MemberCount(uint16 ZoneID);                  // узнаем количество участников группы в указанной зоне Get the number of party members in the zone

    CBattleEntity* GetLeader();                         // узнаем лидера группы
    CBattleEntity* GetSyncTarget();                     // узнаем цель синхронизации Learn the purpose of synchronization
    CBattleEntity* GetQuaterMaster();                   // узнаем владельца сокровищ Find out the owner of the treasure
    CBattleEntity* GetMemberByName(int8* MemberName);   // Returns entity pointer for member name string

	void DisbandParty();								// распускаем группу
	void ReloadParty();                                 // Reload the card group for all members of the group
	void ReloadPartyMembers(CCharEntity* PChar);        // Reload party status for the selected character
	void ReloadTreasurePool(CCharEntity* PChar);

    void AddMember(CBattleEntity* PEntity);             // добавляем персонажа в группу Add character to the group
    void RemoveMember(CBattleEntity* PEntity);          // удаление персонажа из группы Remove the character from the group
    void AssignPartyRole(int8* MemberName, uint8 role); // назначаем роли участникам группы Assign roles to team members
    void DisableSync();                                 // Disable level sync
	void SetSyncTarget(CBattleEntity* PEntity, uint16 message); // устанавливаем цель синхронизации уровней3 Set the goal of synchronization levels 3
    void RefreshSync();                                 // Refresh your level sync

    void PushPacket(CCharEntity* PPartyMember, uint16 ZoneID, CBasicPacket* packet);		// отправляем пакет всем членам группы, за исключением PPartyMember
                                                                                            // Send the packet to all members of the group, except PPartyMember

	CAlliance* m_PAlliance;

    // ВНИМАНИЕ: НЕ ИЗМЕНЯТЬ ЗНАЧЕНИЯ СПИСКА ВНЕ КЛАССА ГРУППЫ
    // NOTE: Do not change the outside of the class LIST GROUPS

    std::vector<CBattleEntity*> members;                // список участников группы List of group members

private:

	
    uint32    m_PartyID;                                // уникальный ID группы unique ID of
    PARTYTYPE m_PartyType;                              // тип существ, составляющих группу type of creatures that make up the group
	
	CBattleEntity* m_PLeader;                           // лидер группы group leader
	CBattleEntity* m_PSyncTarget;                       // цель синхронизации уровней target synchronization levels
	CBattleEntity* m_PQuaterMaster;                     // владелец сокровищ owner of the treasure


	void SetLeader(CBattleEntity* PEntity);             // устанавливаем лидера группы set the group leader
	void SetQuaterMaster(CBattleEntity* PEntity);       // устанавливаем владельца сокровищ Set the owner of treasures

	void RemovePartyLeader(CBattleEntity* PEntity);     // лидер покидает группу leaves the group leader
};

#endif