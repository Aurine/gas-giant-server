﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _LUATRADECONTAINER_H
#define _LUATRADECONTAINER_H

#include "../../common/cbasetypes.h"
#include "../../common/lua/lunar.h"

#include "../trade_container.h"


class CLuaTradeContainer
{
	CTradeContainer *m_pMyTradeContainer;
public:

	static const int8 className[];
	static Lunar<CLuaTradeContainer>::Register_t methods[];

	CLuaTradeContainer(lua_State*);
	CLuaTradeContainer(CTradeContainer*);

	CTradeContainer* GetTradeContainer()const
	{
		return m_pMyTradeContainer;
	}

	int32 getGil(lua_State*);				// The total amount of gil to trade
	int32 getItem(lua_State*);				// The Item ID of item in the slot to trade
	int32 getItemSubId(lua_State*);			// The Item ID of item in the slot to trade
	int32 getItemQty(lua_State*);			// The total number of items in the slot to trade
	int32 hasItemQty(lua_State*);			// Check if the amount of items in the slot to trade is x
	int32 getSlotQty(lua_State*);			// The number of items in the specified cell
	int32 getItemCount(lua_State*);			// The total number of subjects
	int32 getSlotCount(lua_State*);			// The number of slots with items to trade
	int32 confirmItem(lua_State*);			// Confirm we want to trade this item

    int32 IsTypeEquipment(lua_State*);		// The Item Type of item in the slot to trade
    int32 IsSubTypeAugmented(lua_State*);	// The Item SubType of item in the slot to trade
    int32 notFullCharges(lua_State*);		// Checks if item in the slot to trade is a charged item AND if it has full charges
};

#endif