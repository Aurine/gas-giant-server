/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#include <string.h>
#include "mobskill.h"

CMobSkill::CMobSkill(uint16 id)
{
	m_ID             = id;
	m_FamilyID       = 0;
	m_AnimID         = 0;
	m_Aoe            = 0;
    m_Distance       = 0;
	m_TotalTargets   = 1;
	m_Flag           = 0;
    m_ValidTarget    = 0;
    m_AnimationTime  = 0;
    m_ActivationTime = 0;
	m_Message        = 0;
    m_Param          = 0;
    m_skillchain     = 0;
}

/************************************************************************
*                                                                       *
*  Make sure we have a message for a miss.                              *
*                                                                       *
************************************************************************/

bool CMobSkill::hasMissMsg()
{
    return m_Message == 158 || m_Message == 188 || m_Message == 31 || m_Message == 30;
}

/************************************************************************
*                                                                       *
*  Check if the Mobskill is AoE.                                        *
*                                                                       *
************************************************************************/

bool CMobSkill::isAoE()
{
    return m_Aoe > 0 && m_Aoe < 4;
}

/************************************************************************
*                                                                       *
*  Check if the Mobskill is Conal AoE.                                  *
*                                                                       *
************************************************************************/

bool CMobSkill::isConal()
{
    return m_Aoe == 4;
}

/************************************************************************
*                                                                       *
*  Check if the Mobskill is single target.                              *
*                                                                       *
************************************************************************/

bool CMobSkill::isSingle()
{
    return m_Aoe == 0;
}

/************************************************************************
*                                                                       *
*  Check if the Mobskill is it's job's 2 hour ability.                  *
*                                                                       *
************************************************************************/

bool CMobSkill::isTwoHour()
{
    // ID zero means it was put on mob skill list
    // Flag means this skill is a real two hour
    return m_ID == 0 || m_Flag & SKILLFLAG_TWO_HOUR;
}

/************************************************************************
*                                                                       *
*  Set the Mobskill ID.                                                 *
*                                                                       *
************************************************************************/

void CMobSkill::setID(uint16 id)
{
    m_ID = id;
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's message.                                          *
*                                                                       *
************************************************************************/

void CMobSkill::setMsg(uint16 msg)
{
	m_Message = msg;
}

/************************************************************************
*                                                                       *
*  Set the total number of targets for the Mobskill.                    *
*                                                                       *
************************************************************************/

void CMobSkill::setTotalTargets(uint16 targets)
{
    m_TotalTargets = targets;
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's Family ID.                                        *
*                                                                       *
************************************************************************/

void CMobSkill::setfamilyID(uint16 familyID)
{
	m_FamilyID = familyID;
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's Animation ID.                                     *
*                                                                       *
************************************************************************/

void CMobSkill::setAnimationID(uint16 animID)
{
	m_AnimID = animID;
}

/************************************************************************
*                                                                       *
*  Retrieve the Mobskill's name.                                        *
*                                                                       *
************************************************************************/

const int8* CMobSkill::getName()
{
	return m_name.c_str();
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's name.                                             *
*                                                                       *
************************************************************************/

void CMobSkill::setName(int8* name)
{
	m_name.clear();
	m_name.insert(0,name);
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's AoE type.                                         *
*                                                                       *
************************************************************************/

void CMobSkill::setAoe(uint8 aoe)
{
	m_Aoe = aoe;
}

/************************************************************************
*                                                                       *
*  Set the maximum distance the Mobskill will reach.                    *
*                                                                       *
************************************************************************/

void CMobSkill::setDistance(float distance)
{
	m_Distance = distance;
}

/************************************************************************
*                                                                       *
*  Set the Mobskill's flag.                                             *
*                                                                       *
************************************************************************/

void CMobSkill::setFlag(uint8 flag)
{
	m_Flag = flag;
}

/************************************************************************
*                                                                       *
*  Set TP the Mobskill uses.                                            *
*                                                                       *
************************************************************************/

void CMobSkill::setTP(int16 tp)
{
	m_TP = tp;
}

/************************************************************************

*                                                                       *
*  Set time length of the Mobskill's animation.                         *
*                                                                       *
************************************************************************/

void CMobSkill::setAnimationTime(uint16 AnimationTime)
{
    m_AnimationTime = AnimationTime;
}

/************************************************************************
*                                                                       *
*  Set activation time length the Mobskill takes to use.                *
*                                                                       *
************************************************************************/

void CMobSkill::setActivationTime(uint16 ActivationTime)
{
    m_ActivationTime = ActivationTime;
}

/************************************************************************
*                                                                       *
*  Set valid targets of the Mobskill.                                   *
*                                                                       *
************************************************************************/

void CMobSkill::setValidTargets(uint16 targ)
{
    m_ValidTarget = targ;
}

/************************************************************************
*                                                                       *
*  Get the ID of the Mobskill.                                          *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getID()
{
	return m_ID;
}

/************************************************************************
*                                                                       *
*  Get the family of the Mobskill.                                      *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getfamilyID()
{
	return m_FamilyID;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's animation ID.                                     *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getAnimationID()
{
	return m_AnimID;
}

/************************************************************************
*                                                                       *
*  Get the TP the Mobskill uses.                                        *
*                                                                       *
************************************************************************/

int16 CMobSkill::getTP()
{
	return m_TP;
}

/************************************************************************
*                                                                       *
*  Get the total number of targets for the Mobskill.                    *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getTotalTargets()
{
    return m_TotalTargets;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's message.                                          *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getMsg()
{
	return m_Message;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill message for action.                                 *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getMsgForAction()
{
    uint16 id = getID();
    uint16 messageid = 256 + id;
    uint8 flag = getFlag();
    if (flag == SKILLFLAG_WS)
    {
        switch (id)
        {
            case 190:  // Dimensional Death
                messageid = 255;
                break;
            case 246:  // Shackled Fists
            case 247:  // Foxfire
            case 248:  // Grim Halo
            case 249:  // Netherspikes
            case 250:  // Carnal Nightmare
                messageid = id;
                break;
            case 251:  // Dancing Chains
            case 252:  // Barbed Crescent
                messageid = id+1;
                break;
            case 253:  // Aegis Schism
                messageid = 251;
                break;
            default:
                break;
        }
    }
    return messageid;
}

/************************************************************************
*                                                                       *
*  Get the AoE message of the Mobskill.                                 *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getAoEMsg()
{

    switch(m_Message)
    {
        case 185:
            return 264;
        case 186:
            return 266;
        case 187:
            return 281;
        case 188:
            return 282;
        case 189:
            return 283;
        case 225:
            return 366;
        case 226:
            return 226; // No message for this... I guess there is no aoe TP drain move
        case 103: // Recover HP
        case 102: // Recover HP
        case 238: // Recover HP
        case 306: // Recover HP
        case 318: // Recover HP
            return 24;
        case 242:
            return 277;
        case 243:
            return 278;
        case 284:
            return 284; // Already the AoE message
        case 370:
            return 404;
        case 362:
            return 363;
        case 378:
            return 343;
        case 224: // Recovers MP
            return 276;
        default:
            return m_Message;
    }
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's flag.                                             *
*                                                                       *
************************************************************************/

uint8 CMobSkill::getFlag()
{
    return m_Flag;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's AoE type.                                         *
*                                                                       *
************************************************************************/

uint8 CMobSkill::getAoe()
{
    return m_Aoe;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's maximum distance of effect.                       *
*                                                                       *
************************************************************************/

float CMobSkill::getDistance()
{
    return m_Distance;
}

/************************************************************************
*                                                                       *
*  Get the radius for the Mobskill's Radial AoE.                        *
*                                                                       *
************************************************************************/

float CMobSkill::getRadius()
{
    if(m_Aoe == 2)
    {
        // Centered around target, usually 8'
        return 8.0f;
    }

    return m_Distance;
}

/************************************************************************
*                                                                       *
*  Get parameters for Mobskill.                                         *
*                                                                       *
************************************************************************/

int16 CMobSkill::getParam()
{
    return m_Param;
}

/************************************************************************
*                                                                       *
*  Get the Mobskill's knockback amount.                                 *
*                                                                       *
************************************************************************/

uint8 CMobSkill::getKnockback()
{
    return m_knockback;
}

/************************************************************************
*                                                                       *
*  Check if the Mobskill's message is a damage message.                 *
*                                                                       *
************************************************************************/

bool CMobSkill::isDamageMsg()
{
    return m_Message == 110 || m_Message == 185 || m_Message == 197 || m_Message == 264 || m_Message == 187 || m_Message == 225 || m_Message == 226;
}

/************************************************************************
*                                                                       *
*  Set the parameters of the Mobskill.                                  *
*                                                                       *
************************************************************************/

void CMobSkill::setParam(int16 value)
{
    m_Param = value;
}

/************************************************************************
*                                                                       *
*  Set the knockback of the Mobskill.                                   *
*                                                                       *
************************************************************************/

void CMobSkill::setKnockback(uint8 knockback)
{
    m_knockback = knockback;
}

/************************************************************************
*                                                                       *
*  Get the valid targets of the Mobskill.                               *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getValidTargets()
{
    return m_ValidTarget;
}

/************************************************************************
*                                                                       *
*  Get the animation time for the Mobskill.                             *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getAnimationTime()
{
    return m_AnimationTime;
}

/************************************************************************
*                                                                       *
*  Get the activation time of the Mobskill.                             *
*                                                                       *
************************************************************************/

uint16 CMobSkill::getActivationTime()
{
    return m_ActivationTime;
}

/************************************************************************
*                                                                       *
*  Get the skillchain of the Mobskill.                                  *
*                                                                       *
************************************************************************/

uint8 CMobSkill::getSkillchain()
{
    return m_skillchain;
}

/************************************************************************
*                                                                       *
*  Set the skillchain of the Mobskill.                                  *
*                                                                       *
************************************************************************/

void CMobSkill::setSkillchain(uint8 skillchain)
{
    m_skillchain = skillchain;
}