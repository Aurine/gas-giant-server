-- phpMyAdmin SQL Dump
-- version 3.3.8
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Ven 24 Juin 2011 � 08:10
-- Version du serveur: 6.0.0
-- Version de PHP: 5.2.9-2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de donn�es: `dspdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `npc_dummies`
--

DROP TABLE IF EXISTS `npc_dummies`;
CREATE TABLE IF NOT EXISTS `npc_dummies` (
  `npcid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`npcid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `npc_dummies`
--

INSERT INTO `npc_dummies` VALUES('17187456'); -- Aaveleon, West Ronfaure
INSERT INTO `npc_dummies` VALUES('17187458'); -- Zovriace, West Ronfaure
INSERT INTO `npc_dummies` VALUES('17387987'); -- Quemaricond, Davoi
INSERT INTO `npc_dummies` VALUES('17461503'); -- Novalmauge, Bostaunieux Oubliette
INSERT INTO `npc_dummies` VALUES('17719326'); -- Raminel, Southern San d'Oria
INSERT INTO `npc_dummies` VALUES('17719328'); -- Glenne, Southern San d'Oria
INSERT INTO `npc_dummies` VALUES('17748033'); -- Fariel, Metalworks
INSERT INTO `npc_dummies` VALUES('17780794'); -- Navisse, Lower Jeuno
INSERT INTO `npc_dummies` VALUES('17784896'); -- Red Ghost, Port Jeuno
INSERT INTO `npc_dummies` VALUES('17801237'); -- Thali Mhobrum, Kazham
INSERT INTO `npc_dummies` VALUES('17801288'); -- Lulupp, Kazham
INSERT INTO `npc_dummies` VALUES('17801289'); -- Kukupp, Kazham
INSERT INTO `npc_dummies` VALUES('17801290'); -- Mumupp, Kazham
INSERT INTO `npc_dummies` VALUES('17801291'); -- Roropp, Kazham
INSERT INTO `npc_dummies` VALUES('17801294'); -- Tatapp, Kazham
INSERT INTO `npc_dummies` VALUES('17801296'); -- Lalapp, Kazham
INSERT INTO `npc_dummies` VALUES('17809436'); -- Louartain, Norg
INSERT INTO `npc_dummies` VALUES('17809437'); -- Shivivi, Norg
INSERT INTO `npc_dummies` VALUES('17809438'); -- Deigoff, Norg
INSERT INTO `npc_dummies` VALUES('17809439'); -- Oruga, Norg
INSERT INTO `npc_dummies` VALUES('17809440'); -- Parlemaille, Norg
INSERT INTO `npc_dummies` VALUES('17809441'); -- Keal, Norg
INSERT INTO `npc_dummies` VALUES('17809442'); -- Paito-Maito, Norg