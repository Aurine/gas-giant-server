---------------------------------------------------------------------------------------------------
-- func: @addzeni <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Adds the specified amount of Zeni to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@addzeni <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addZeni(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addZeni(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addzeni <amount> <player>" );
        end
    end
end;