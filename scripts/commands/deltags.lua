---------------------------------------------------------------------------------------------------
-- func: @deltags <amount> <player>
-- auth: Forgottenandlost
-- desc: Deletes the specified amount of Imperial ID (assault) tags from the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,amount,target)

    if (spellId == nil) then
        player:PrintToPlayer( "You must enter a valid spellID." );
        player:PrintToPlayer( "@deltags <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delTags(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delTags(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@deltags <amount> <player>" );
        end
    end
end;