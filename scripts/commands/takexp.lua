---------------------------------------------------------------------------------------------------
-- func: @takexp <amount> <player>
-- auth: Forgottenandlost
-- desc: Removes experience points from the target player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

print( 'Exp amount: ' .. tostring( amount ) );

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@takexp <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delExp(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delExp(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@takexp <amount> <player>" );
        end
    end
end;