---------------------------------------------------------------------------------------------------
-- func: @addcp <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Adds the specified amount of Conquest Points to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@addcp <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addCP(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addCP(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addcp <amount> <player>" );
        end
    end
end;