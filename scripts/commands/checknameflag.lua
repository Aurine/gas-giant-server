---------------------------------------------------------------------------------------------------
-- func: @checknameflag <flag> <target>
-- auth: Forgottenandlost
-- desc: check if the specified flags is on/off (true/false)
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 4,
    parameters = "ss"
};

local flag_list = {}         -- uint32, Hex
-- flag_list["FLAG_??"]          = 0x00000001;
flag_list["FLAG_INEVENT"]     = 0x00000002; --
flag_list["FLAG_INVIS"]       = 0x00000004; --
-- flag_list["FLAG_??"]          = 0x00000008;    
-- flag_list["FLAG_??"]          = 0x00000010;
-- flag_list["FLAG_??"]          = 0x00000020;
flag_list["FLAG_CHOCOBO"]     = 0x00000040; --
-- flag_list["FLAG_??"]          = 0x00000080;
-- flag_list["FLAG_DESPAWN"]     = 0x00000100;
flag_list["FLAG_WALLHACK"]    = 0x00000200; --
-- flag_list["FLAG_??"]          = 0x00000400;
flag_list["FLAG_INVITE"]      = 0x00000800; --
flag_list["FLAG_ANON"]        = 0x00001000; --
flag_list["FLAG_ORANGE"]      = 0x00002000; -- Orange Name
flag_list["FLAG_UNKNOWN"]     = 0x00002000; -- Orange Name
flag_list["FLAG_AWAY"]        = 0x00004000;
-- flag_list["FLAG_??"]          = 0x00008000;
flag_list["FLAG_PLAYONLINE"]  = 0x00010000; -- POL icon
flag_list["FLAG_LINKSHELL"]   = 0x00020000;
flag_list["FLAG_DC"]          = 0x00040000;
-- flag_list["FLAG_TARGETABLE"] = 0x00080000;
-- flag_list["FLAG_??"]         = 0x00100000;
-- flag_list["FLAG_TEMP"]       = 0x00200000;
-- flag_list["FLAG_TEMP"]       = 0x00400000;
-- flag_list["FLAG_HIDDEN_GM"]  = 0x00800000;
flag_list["FLAG_SENIOR"]      = 0x01000000;
flag_list["FLAG_LEAD"]        = 0x02000000;
flag_list["FLAG_PRODUCER"]    = 0x03000000;
flag_list["FLAG_GM"]          = 0x04000000;
flag_list["FLAG_GM_SUPPORT"]  = 0x04000000;
flag_list["FLAG_GM_SENIOR"]   = 0x05000000; -- FLAG_GM+0x01000000
flag_list["FLAG_GM_LEAD"]     = 0x06000000; -- FLAG_GM+0x02000000
flag_list["FLAG_GM_PRODUCER"] = 0x07000000; -- FLAG_GM+0x03000000
-- flag_list["FLAG_??"]          = 0x08000000;
-- flag_list["FLAG_??"]          = 0x10000000;
-- flag_list["FLAG_??"]          = 0x20000000;
-- flag_list["FLAG_??"]          = 0x40000000;
flag_list["FLAG_BAZAAR"]      = 0x80000000;
function getFlagList(i)
    return flag_list[i];
end;

function toHex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

function onTrigger(player, FlagID, target)
    local flag = 0;

    if (FlagID == nil) then
        player:PrintToPlayer("You must enter valid nameflag to check!");
        player:PrintToPlayer( "@checkflag <flag> <target>" );
        return;
    else
        flag = getFlagList(FlagID);
    end

    if (target == nil) then
        if (player:checkNameFlags(flag) == true) then
            player:PrintToPlayer( string.format( "'%s' On", toHex(flag) ) );
        elseif (player:checkNameFlags(flag) == false) then
            player:PrintToPlayer( string.format( "'%s' Off", toHex(flag) ) );
        else
            player:PrintToPlayer( "Unknown error - invalid flag?" );
        end
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            if (targ:checkNameFlags(flag) == true) then
                player:PrintToPlayer( string.format( "'%s' On", toHex(flag) ) );
            elseif (targ:checkNameFlags(flag) == false) then
                player:PrintToPlayer( string.format( "'%s' Off", toHex(flag) ) );
            else
                player:PrintToPlayer( "Unknown error - invalid flag?" );
            end
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@checkflag <flag> <target>" );
        end
    end

end;
