---------------------------------------------------------------------------------------------------
-- func: @delis <amount> <player>
-- auth: Compmike
-- desc: Deletes the specified amount of Imperial Standing to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,amount,target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid amount." );
        player:PrintToPlayer( "@delis <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delImperialStanding(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delImperialStanding(amount);
        else
            player:PrintToPlayer( "You must enter a valid Player's Name." );
            player:PrintToPlayer( "@delis <amount> <player>" );
        end
    end
end;