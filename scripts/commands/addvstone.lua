---------------------------------------------------------------------------------------------------
-- func: @addvstone <amount> <player>
-- auth: Forgottenandlost
-- desc: Adds the specified amount of Voidstones to the player's stock
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@addvstone <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addVstone(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addVstone(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addvstone <amount> <player>" );
        end
    end
end