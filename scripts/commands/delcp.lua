---------------------------------------------------------------------------------------------------
-- func: @delcp <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Deletes the specified amount of Conquest Points from the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@delcp <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delCP(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delCP(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delcp <amount> <player>" );
        end
    end
end;