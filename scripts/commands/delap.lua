---------------------------------------------------------------------------------------------------
-- func: @delap <amount> <region> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Deletes the specified amount of Assault Points to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "iis"
};

function onTrigger(player,amount,region,target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@delap <amount> <region> <player>" );
        return;
    end

    if (region == nil) then
        player:PrintToPlayer( "You must enter a valid Region ID." );
        player:PrintToPlayer( "@delap <amount> <region> <player>" );
        player:PrintToPlayer( "Regions: 0 - Leujaoam, 1 - Mamool, 2 - Lebros" );
        player:PrintToPlayer( "Regions: 3 - Periqia, 4 - Ilrusi, 5 - Nyzul" );
        return;
    end

    if (target == nil) then
        player:delAssaultPoint(amount, region);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delAssaultPoint(amount, region);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delap <amount> <region> <player>" );
        end
    end
end;