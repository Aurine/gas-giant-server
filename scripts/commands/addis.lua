---------------------------------------------------------------------------------------------------
-- func: @addis <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Adds the specified amount of Imperial Standing to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@addis <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addImperialStanding(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addImperialStanding(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addis <amount> <player>" );
        end
    end
end;