---------------------------------------------------------------------------------------------------
-- func: title
-- auth: <Unknown>, modified by Forgottenandlost
-- desc: Sets a players title.
---------------------------------------------------------------------------------------------------

cmdprops =
{
	permission = 1,
	parameters = "is"
};

function onTrigger(player, titleId, target)
	local targ = GetPlayerByName(target);
	if (target == nil) then
		target = player:getName();
		return;
	end

	if (titleId == nil) then
		player:PrintToPlayer("You must enter a valid title id.");
		return
	end

	targ:addTitle( titleId );
end