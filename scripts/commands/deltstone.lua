---------------------------------------------------------------------------------------------------
-- func: @deltstone <amount> <player>
-- auth: Forgottenandlost
-- desc: Deletes the specified amount of Traverser Stones from the player's stock
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,amount,target)

    if (spellId == nil) then
        player:PrintToPlayer( "You must enter a valid spellID." );
        player:PrintToPlayer( "@deltstone <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delTstone(spellId);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delTstone(spellId);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@deltstone <amount> <player>" );
        end
    end
end;