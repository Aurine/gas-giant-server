---------------------------------------------------------------------------------------------------
-- func: @addkeyitem <ID> <player>
-- auth: <Unknown>, modified by Forgottenandlost
-- desc: Adds a key item to the player.
---------------------------------------------------------------------------------------------------

require("scripts/globals/keyitems");

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, keyId, target)

    if (keyId == nil or tonumber(keyId) == 0 or tonumber(keyId) == nil or keyId == 0) then
        player:PrintToPlayer("You must enter a valid keyitem ID.");
        player:PrintToPlayer( "@addkeyitem <ID> <player>" );
        return;
    end

    if (target == nil) then
        local TextIDs = "scripts/zones/" .. player:getZoneName() .. "/TextIDs";
        package.loaded[TextIDs] = nil;
        require(TextIDs);
        player:addKeyItem( keyId );
        player:messageSpecial( KEYITEM_OBTAINED, keyId );
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            local TextIDs = "scripts/zones/" .. targ:getZoneName() .. "/TextIDs";
            package.loaded[TextIDs] = nil;
            require(TextIDs);
            targ:addKeyItem( keyId );
            targ:messageSpecial( KEYITEM_OBTAINED, keyId );
            player:PrintToPlayer( string.format( "Keyitem ID '%u' added to player!", keyId ) );
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addkeyitem <ID> <player>" );
        end
    end
end;