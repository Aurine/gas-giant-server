---------------------------------------------------
-- Mobskill: Horrid Roar
--
-- Removes beneficial effects from a single target
-- and reduces enmity.
-- Type: Enfeebling
-- Utsusemi/Blink absorb: Wipes shadows
-- Range: Melee or radial
-- Notes: The effect varies slightly by which wyrm
-- used this ability:
-- Nidhogg - All buffs removed including food,
-- moderate enmity reset 
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------------

function OnMobSkillCheck(target,mob,skill)
    if(target:isBehind(mob, 48) == true) then
        return 1;
    elseif (mob:AnimationSub() ~= 0) then
        return 1;
    end
    return 0;
end;

function OnMobWeaponSkill(target, mob, skill)

    local dispel =  target:dispelAllStatusEffect(bit.bor(EFFECTFLAG_DISPELABLE, EFFECTFLAG_FOOD));

    if(dispel == 0) then
        skill:setMsg(MSG_NO_EFFECT); -- No effect
    else
        skill:setMsg(MSG_DISAPPEAR_NUM);
    end

    mob:lowerEnmity(target, 45);

    return dispel;
end;