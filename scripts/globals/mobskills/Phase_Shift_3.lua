---------------------------------------------------
-- Mobskill: Phase Shift 3
--
-- A shockwave deals damage to targets in an area
-- of effect. Additional effect: Stun + Bind
-- Family: Structures
-- Type: Physical
-- Can be dispelled: N/A
-- Utsusemi/Blink absorb: 2-3 shadows
-- Range: 30' radial
-- Notes: Used only by Exoplates as a reaction when
-- it loses every 33% of its health. The attack
-- gets deadlier each time, ranging from 600 to
-- 1300 damage. 
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------------

function OnMobSkillCheck(target,mob,skill)
	return 1;
end;

function OnMobWeaponSkill(target, mob, skill)
	local numhits = 1;
	local accmod = 1;
	local dmgmod = 7;
	local info = MobPhysicalMove(mob,target,skill,numhits,accmod,dmgmod,TP_DMG_VARIES,4,4,4);
	local dmg = MobFinalAdjustments(info.dmg,mob,skill,target,MOBSKILL_PHYSICAL,MOBPARAM_SLASH,MOBPARAM_2_SHADOW);
    MobPhysicalStatusEffectMove(mob, target, skill, EFFECT_STUN, 1, 0, 15);
    MobPhysicalStatusEffectMove(mob, target, skill, EFFECT_BIND, 1, 0, 30);
	target:delHP(dmg);
    mob:setUnkillable(false);
	return dmg;
end;