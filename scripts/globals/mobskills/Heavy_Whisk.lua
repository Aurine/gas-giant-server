---------------------------------------------
-- Mob Skill: Heavy Whisk
-- Type: Physical (Blunt)
-- Description: Damage varies with TP.
-- Utsusemi/Blink absorb: Ignores shadows
-- Range: Melee
---------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------
function OnMobSkillCheck(target,mob,skill)
    return 0;
end;

function OnMobWeaponSkill(target, mob, skill)

	local numhits = 1;
	local accmod = 1;
	local dmgmod = 2;
	local info = MobPhysicalMove(mob,target,skill,numhits,accmod,dmgmod,TP_NO_EFFECT);
	local dmg = MobFinalAdjustments(info.dmg,mob,skill,target,MOBSKILL_PHYSICAL,MOBPARAM_H2H,MOBPARAM_IGNORE_SHADOWS);
	target:delHP(dmg);
	return dmg;
end;