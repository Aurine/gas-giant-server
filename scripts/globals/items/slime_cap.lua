-----------------------------------------
--	ID: 27756
--	Slime Cap
--	DEF:1 HP+3 Dispense: Slimeulation Candy
-----------------------------------------

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
local result = 0;
	if (target:getFreeSlotsCount() == 0) then
		result = 308;
	end
return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addItem(6187,1);
end;