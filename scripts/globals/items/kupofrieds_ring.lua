-----------------------------------------
-- ID: 15840
-- Item: Kupofrieds Ring
-- Experience point bonus
-----------------------------------------
-- Bonus: +100%
-- Duration: 1440 min
-- Max bonus: 6000 exp
-----------------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
	local result = 0;
	if (target:hasStatusEffect(EFFECT_DEDICATION) == true) then
		result = 56;
	end
	return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	-- target:addStatusEffect(EFFECT_DEDICATION,100,0,86400);
	-- target:addMod(MOD_DEDICATION_CAP, 6000);
	target:addStatusEffect(EFFECT_DEDICATION,100,0,86400,0,6000);
end;