-----------------------------------------
-- ID: 4495
-- Item: Chunk of Goblin Chocolate
-- Food Effect: 3 Min, All Races
-----------------------------------------
-- Health Regen While Healing 5
-- Lizard Killer 10
-- Resist Petrify 10
-- http://wiki.ffxiclopedia.org/wiki/Goblin_Chocolate
-- http://wiki.bluegartr.com/bg/Gob._Chocolate
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
local result = 0;
	if (target:hasStatusEffect(EFFECT_FOOD) == true or target:hasStatusEffect(EFFECT_FIELD_SUPPORT_FOOD) == true) then
		result = 246;
	end
return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addStatusEffect(EFFECT_FOOD,0,0,180,4495);
end;

-----------------------------------------
-- onEffectGain Action
-----------------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_HPHEAL, 5);
	target:addMod(MOD_LIZARD_KILLER, 10);
	target:addMod(MOD_PETRIFYRES, 10);
end;

-----------------------------------------
-- onEffectLose Action
-----------------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_HPHEAL, 5);
	target:delMod(MOD_LIZARD_KILLER, 10);
	target:delMod(MOD_PETRIFYRES, 10);
end;
