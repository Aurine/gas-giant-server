-----------------------------------------
-- ID: 4324
-- Item: Chunk of Hobgoblin Chocolate
-- Food Effect: 5Min, All Races
-----------------------------------------
-- Health Regen While Healing 7
-- Lizard Killer 14
-- Petrify Resist 14
-- Guestimated amounts based on these 3 links and fact that it is HQ of Goblin Chocolate
-- BG says NQ has healHP of 5 and resist of 10, doesn't have amounts for HQ resist.
-- http://wiki.ffxiclopedia.org/wiki/Hobgoblin_Chocolate
-- http://wiki.ffxiclopedia.org/wiki/Goblin_Chocolate
-- http://wiki.bluegartr.com/bg/Gob._Chocolate
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
local result = 0;
	if (target:hasStatusEffect(EFFECT_FOOD) == true or target:hasStatusEffect(EFFECT_FIELD_SUPPORT_FOOD) == true) then
		result = 246;
	end
return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addStatusEffect(EFFECT_FOOD,0,0,300,4324);
end;

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_HPHEAL, 7);
	target:addMod(MOD_LIZARD_KILLER, 14);
	target:addMod(MOD_PETRIFYRES, 14);
end;

-----------------------------------------
-- onEffectLose Action
-----------------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_HPHEAL, 7);
	target:delMod(MOD_LIZARD_KILLER, 14);
	target:delMod(MOD_PETRIFYRES, 14);
end;
