-----------------------------------
-- Ability: Devotion
-- Sacrifices HP to grant a party
-- member the same amount in MP.
-- Merits: Increases MP restored by
-- 5% for each rank beyond the
-- first.
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)
	local merit = target:getMerit(MERIT_DEVOTION); -- 5% increase to MP past 1st Merit
	if (player:getHP() > 3) then
		effectPower = player:getHP()/4;
		player:delHP(effectPower);
		effectPower = effectPower + (effectPower * ((merit * .01) - .05));
		target:addMP(effectPower);
	end
end;