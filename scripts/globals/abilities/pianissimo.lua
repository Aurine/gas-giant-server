-----------------------------------
-- Ability: Pianissimo
-- Limits area of effect of next
-- song to a single target. 
-----------------------------------
 
require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)
	player:addStatusEffect(EFFECT_PIANISSIMO,0,0,60);
end;