-----------------------------------
-- Ability: Hide
-- Allows you to evade enemies by
-- hiding.
-- Gives you the effect of
-- Invisible. Allows you to lose
-- aggro on certain types of
-- enemies. Generally works on
-- monsters that don't track by
-- scent, regardless of detection
-- method. Can work on monsters
-- that track by scent if the
-- proper conditions are met
-- (double rain weather, crossing
-- over water, etc.) Will reset
-- Thief Enmity if used when at the
-- top of the hate list on these
-- enemies.
-- You can Sneak Attack from any
-- direction when Hide is active on
-- targets where Hide works. 
-----------------------------------
 
require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)

	local duration = math.random(30, 300);
	duration = duration + player:getMod(MOD_HIDE);

	player:addStatusEffect(EFFECT_HIDE,1,0,(duration * SNEAK_INVIS_DURATION_MULTIPLIER));
	return EFFECT_HIDE;
end;