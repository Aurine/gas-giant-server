-----------------------------------
-- Ability: Rampart
-- Grants a Magic Shield effect and
-- enhances defense for party
-- members within area of effect.
-- Merit Iron Will decreases Spell
-- Interruption Rate during Rampart
-- by 19% per merit level. 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)
	local duration = 30;
	local merits = player:getMerit(MERIT_IRON_WILL);
	local spellIntRate = 0;
	duration = duration + player:getMod(MOD_RAMPART_DURATION);

	if (merits > 0) then
		spellIntRate = merits;
	end
	target:addStatusEffect(EFFECT_MAGIC_SHIELD,1,0,duration, 0, spellIntRate);
end;