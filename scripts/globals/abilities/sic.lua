-----------------------------------
-- Ability: Sic
-- Commands the charmed Pet to make
-- a random special attack.
--
-- If the mob is a spell casting
-- type it will cast a spell
-- instead of using a special
-- attack.
--
-- If used BEFORE the mob has
-- enough TP stored, 100%, it will
-- simply wait until it has enough
-- TP and use the special move
-- then. For mobs like Hecteyes
-- this can result in a very
-- powerful spell being used for
-- nice damage right away. 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	if (player:getPet() == nil) then
		return MSGBASIC_REQUIRES_A_PET,0;
	else
		if (player:getPet():getHP() == 0) then
			return MSGBASIC_UNABLE_TO_USE_JA,0;
		elseif (player:getPet():getTarget() == nil) then
			return MSGBASIC_PET_CANNOT_DO_ACTION,0;
		elseif (not player:getPet():hasTPMoves()) then
			return MSGBASIC_UNABLE_TO_USE_JA,0;
		else
			return 0,0;
		end
	end
end;

function OnUseAbility(player, target, ability)
end;
