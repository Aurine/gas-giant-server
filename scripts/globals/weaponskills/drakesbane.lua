-----------------------------------	
-- Weaponskill: Drakesbane	
-- Polearm weapon skill	
-- Skill level: N/A	
-- Delivers a fourfold attack.
-- Chance of params.critical hit
-- varies with TP. Ryunohige:
-- Aftermath effect varies with TP.	
-- Available only after completing
-- the Unlocking a Myth (Dragoon)
-- quest.	
-- Aligned with the Flame Gorget &
-- Light Gorget.	
-- Aligned with the Flame Belt &
-- Light Belt.	
-- Element: None	
-- Modifiers: STR:50%	
-- 100%TP    200%TP    300%TP	
-- 1.00      1.00      1.00	
-----------------------------------	

require("scripts/globals/status");
require("scripts/globals/settings");
require("scripts/globals/weaponskills");

-----------------------------------	

function OnUseWeaponSkill(player, target, wsID)	

	local params = {};
	params.numHits = 4;
	params.ftp100 = 1; params.ftp200 = 1; params.ftp300 = 1;
	params.str_wsc = 0.5; params.dex_wsc = 0.0; params.vit_wsc = 0.0; params.agi_wsc = 0.0; params.int_wsc = 0.0; params.mnd_wsc = 0.0; params.chr_wsc = 0.0;
	params.crit100 = 0.1; params.crit200 = 0.3; params.crit300 = 0.5;
	params.canCrit = true;
	params.acc100 = 0.0; params.acc200= 0.0; params.acc300= 0.0;
	params.atkmulti = 1;
	local damage, criticalHit, tpHits, extraHits = doPhysicalWeaponskill(player, target, params);


	if((player:getEquipID(SLOT_MAIN) == 19004) and (player:getMainJob() == JOB_DRG)) then
		if(damage > 0) then	

--		AFTERMATH LV1		

		if ((player:getTP() >= 100) and (player:getTP() <= 110)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 10, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 111) and (player:getTP() <= 120)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 11, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 121) and (player:getTP() <= 130)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 12, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 131) and (player:getTP() <= 140)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 13, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 141) and (player:getTP() <= 150)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 14, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 151) and (player:getTP() <= 160)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 15, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 161) and (player:getTP() <= 170)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 16, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 171) and (player:getTP() <= 180)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 17, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 181) and (player:getTP() <= 190)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 18, 0, 180, 0, 1); 
	elseif ((player:getTP() >= 191) and (player:getTP() <= 199)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 19, 0, 180, 0, 1); 

			
--		AFTERMATH LV2

	elseif ((player:getTP() >= 200) and (player:getTP() <= 210)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 24, 0, 180, 0, 1);
	elseif ((player:getTP() >= 211) and (player:getTP() <= 219)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 28, 0, 180, 0, 1);
	elseif ((player:getTP() >= 221) and (player:getTP() <= 229)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 32, 0, 180, 0, 1);
	elseif ((player:getTP() >= 231) and (player:getTP() <= 239)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 36, 0, 180, 0, 1);
	elseif ((player:getTP() >= 241) and (player:getTP() <= 249)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 40, 0, 180, 0, 1);
	elseif ((player:getTP() >= 251) and (player:getTP() <= 259)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 44, 0, 180, 0, 1);
	elseif ((player:getTP() >= 261) and (player:getTP() <= 269)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 48, 0, 180, 0, 1);
	elseif ((player:getTP() >= 271) and (player:getTP() <= 279)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 52, 0, 180, 0, 1);
	elseif ((player:getTP() >= 281) and (player:getTP() <= 289)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 56, 0, 180, 0, 1);
	elseif ((player:getTP() >= 291) and (player:getTP() <= 299)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 59, 0, 180, 0, 1);
			
--		AFTERMATH LV3

		elseif ((player:getTP() == 300)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV3, 45, 0, 120, 0, 1);

			end
		end
	end	
	local main = player:getEquipID(SLOT_MAIN);
	local aftermath = 0;
	local tp = player:getTP();
	local duration = 0;
	local subpower = 0;

	if (main == 19004) then
		aftermath = 1;
	elseif (main == 19073) then
		aftermath = 1;
	elseif (main == 19093) then
		aftermath = 1;
	elseif (main == 19625) then
		aftermath = 1;
		damage = damage * 1.15;
	elseif (main == 19723) then
		aftermath = 1;
		damage = damage * 1.15;
	elseif (main == 19832) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 19961) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 20927) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 20928) then
		aftermath = 1;
		damage = damage * 1.3;
	end

	if (aftermath == 1) then
		if (tp == 300) then
			player:delStatusEffect(EFFECT_AFTERMATH_LV1);
			player:delStatusEffect(EFFECT_AFTERMATH_LV2);
			player:delStatusEffect(EFFECT_AFTERMATH_LV3);
			if (main == 19004) then
				duration = 120;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,14,0,duration,0,40);
			elseif (main == 19073 or main == 19093 or main == 19625) then
				duration = 180;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,14,0,duration,0,60);
			elseif (main == 19723 or main == 19832 or main == 19961) then
				duration = 180;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,15,0,duration,0,20);
			end
		elseif (tp >= 200) then
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				player:delStatusEffect(EFFECT_AFTERMATH_LV1);
				player:delStatusEffect(EFFECT_AFTERMATH_LV2);
				if (main == 19004) then
					duration = 90;
					subpower = math.floor(2 * (tp / 5) - 60);
				elseif (main == 19073 or main == 19093 or main == 19625) then
					duration = 120;
					subpower = math.floor(3 * (tp / 5) - 90);
				elseif (main == 19723 or main == 19832 or main == 19961) then
					duration = 120;
					subpower = math.floor((tp * .6) - 80);
				end
				player:addStatusEffect(EFFECT_AFTERMATH_LV2,14,0,duration,0,subpower);
			end
		else
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				if (player:hasStatusEffect(EFFECT_AFTERMATH_LV2) == false) then
					player:delStatusEffect(EFFECT_AFTERMATH_LV1);
					if (main == 19004) then
						duration = 60;
						subpower = math.floor(tp / 10);
					elseif (main == 19073 or main == 19093 or main == 19625) then
						duration = 90;
						subpower = math.floor(3 * (tp / 20));
					elseif (main == 19723 or main == 19832 or main == 19961) then
						duration = 90;
						subpower = math.floor((tp / 10) + 20);
					end
					player:addStatusEffect(EFFECT_AFTERMATH_LV1,14,0,duration,0,subpower);
				end
			end
		end
	end

	return tpHits, extraHits, criticalHit, damage;

end
