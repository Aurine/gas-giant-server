-----------------------------------
-- 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/conquest");
require("scripts/globals/common");

-----------------------------------
-- Event IDs (per zone)
-----------------------------------
-- HuntEvent_RuLude_Gardens       = ;
-- HuntEvent_Northern_San_dOria   = ;
HuntEvent_Bastok_Mines         = 1500;
-- HuntEvent_Port_Windurst        = ;
-- HuntEvent_Kazham               = ;
-- HuntEvent_Norg                 = ;
-- HuntEvent_Rabao                = ;
-- HuntEvent_Tavnazian_Safehold   = ;
-- HuntEvent_Aht_Urhgan_Whitegate = ;
-- HuntEvent_Nashmau              = ;
-- HuntEvent_Southern_San_dOria_S = ;
-- HuntEvent_Bastok_Markets_S     = ;
-- HuntEvent_Windurst_Waters_S    = ;

----------------------------------
-- Start Hunt Registry onTrigger
----------------------------------

function startHunt(eventid,player)
    -- HuntStatus flags:
    -- 0 = Ok to flag new Hunt!
    -- 1 = Cannot hunt because a FoV/GoV page is active, offers to cancel.
    -- 2 = Hunt already active, give message informing player they may cancel to start another.
    -- 3+ = invalid value, will either reload menu with no message behave as 0~2 did.
    local HuntStatus = 0;
    local ScyldsBalance = 0xFA; -- Max val 0x0FF but retail caps to 0x0FA.
    local DoubleParam = 0;
    local AvailableZones = 0; -- Max value of 0x7FFFFFFF which disables ALL.
    local AvailableHunts = 0; -- Max value of 0x7FFFFFFF which disables ALL.
    local TargetMOB = 200; -- Our test subject, 200, is Leaping Lizzy!
    local ScyldsCost = 0;
    local RequiredKills = 1;    

    if ((player:getVar("fov_regimeid") >= 1) or (player:getVar("gov_regimeid") >= 1)) then
        HuntStatus = 1;
    elseif (player:getVar("hunt_id") >= 1) then
        HuntStatus = 2;
    end
    DoubleParam = HuntStatus+(ScyldsBalance*65536);
    player:PrintToPlayer( string.format( "Value of param is: '%u' ", DoubleParam ) );
    player:startEvent(eventid,DoubleParam,AvailableZones,AvailableHunts,0,0,TargetMOB,ScyldsCost,RequiredKills);
end;

--[[ 
-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    printf("CSID: %u",csid);
    printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    printf("CSID: %u",csid);
    printf("RESULT: %u",option);
    -- if (option == 1) then
    -- elseif (option == 2) then
    -- elseif (option == 3) then
    -- else
    if (option == 4) then
        player:setVar("fov_regimeid",0);
        player:setVar("fov_numkilled1",0);
        player:setVar("fov_numkilled2",0);
        player:setVar("fov_numkilled3",0);
        player:setVar("fov_numkilled4",0);
        -- player:setVar("gov_regimeid",0);
        -- player:setVar("gov_numkilled1",0);
        -- player:setVar("gov_numkilled2",0);
        -- player:setVar("gov_numkilled3",0);
        -- player:setVar("gov_numkilled4",0);
    end
end;
 ]]