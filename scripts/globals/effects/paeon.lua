-----------------------------------
--  EFFECT_PAEON
-- It bestows an effect similar to
-- Regen for party members within
-- range, restoring a small amount
-- of HP every tick for as long as
-- the effect lasts.
-----------------------------------

require("scripts/globals/status");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_REGEN, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_REGEN, effect:getPower());
end;