-----------------------------------
--	EFFECT_SCARLET_DELIRIUM_I
--	(Charging)
-- Channels dmg taken into enhanced
-- atk and magic atk. Dmg proportional
-- to next hit taken hp loss / 2.
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;