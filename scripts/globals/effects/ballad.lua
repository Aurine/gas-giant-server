-----------------------------------
--  EFFECT_BALLAD
-- getPower returns the TIER
-- DO NOT ALTER ANY OF THE EFFECT
-- VALUES! DO NOT ALTER EFFECT
-- POWER! 
-----------------------------------
require("scripts/globals/status");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
    target:addMod(MOD_REFRESH,effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
    target:delMod(MOD_REFRESH,effect:getPower());
end;