-----------------------------------
--
-- 	EFFECT_SIGIL
--
--
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
    target:addMod(MOD_DEF,15);
    target:addMod(MOD_EVA,15);
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
    local pow = effect:getPower();
    -- Todo: Only works below a certain %
    if (pow == 1 or pow == 5 or pow == 9 or pow == 13) then
        target:addHP(1);
    elseif (pow == 2 or pow == 6 or pow == 10 or pow == 14) then
        target:addMP(1);
    elseif (pow == 3 or pow == 7 or pow == 11 or pow == 15) then
        target:addHP(1);
        target:addMP(1);
    end
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
    target:delMod(MOD_DEF,15);
    target:delMod(MOD_EVA,15);
end;