-----------------------------------
--	EFFECT_DRAIN_SAMBA_II
-- Inflicts the next target you
-- strike with Drain daze, allowing
-- all those engaged in battle with
-- it to drain its HP. 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/magic");
 
-----------------------------------
-- onEffectGain Action
-----------------------------------
function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------
 
function onEffectLose(target,effect)
end;