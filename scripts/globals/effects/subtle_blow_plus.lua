-----------------------------------
--	EFFECT_SUBTLE_BLOW_PLUS
-- Reduces amount of TP gained by
-- enemies when striking them.
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_SUBTLE_BLOW, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_SUBTLE_BLOW, effect:getPower());
end;