-----------------------------------
--  
-- 	EFFECT_LEVEL_SYNC
--
-- 	Level Sync is a party
--  leader-activated system whereby
--  the level of all party members
--  is restricted to that of a
--  designated player. When this
--  feature is activated, all
--  members of the party will be
--  able to receive an amount of
--  experience points corresponding
--  to the level of the designee,
--  regardless of any initial
--  disparity between them.
--
--  Level Sync will remain in
--  effect only within the area in
--  which it is activated, and will
--  automatically be deactivated
--  for all members should either
--  the party leader or the
--  designee leave the area. For
--  all other players jumping to
--  new areas, synchronization will
--  be deactivated only for
--  themselves. 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:levelRestriction(effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:levelRestriction(0);
	target:disableLevelSync();
end;