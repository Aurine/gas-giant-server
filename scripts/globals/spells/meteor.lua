-----------------------------------------
-- Spell: Meteor
-- Deals non-elemental damage to an enemy.
-- MP Cost: 418
-----------------------------------------

require("/scripts/globals/settings");
require("scripts/globals/magic");
require("scripts/globals/status");
require("/scripts/globals/monstertpmoves");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	-- TODO: Correct message is "Incorrect job, job level too low, or required ability not activated."  Unable to locate this in our basic or system message functions.
	-- The client blocks the spell via menus, but it can still be cast via text commands, so we have to block it here, albiet with the wrong message.
	if (caster:hasStatusEffect(EFFECT_ELEMENTAL_SEAL) == true) then
		return 0;
	else
		return MSGBASIC_STATUS_PREVENTS;
	end
end;

function onSpellCast(caster,target,spell)

	-- Calculate raw damage
	-- Byrthnoth @ Random Facts Thread: Magic @ BG:
	-- Spell Base Damage = MAB/MDB*floor(Int + Elemental Magic Skill/6)*3.5
	-- NOT AFFECTED BY DARK BONUSES (bonus ETC)
	-- I'll just point this out again. It can't resist, there's no dINT, and the damage is non-elemental.
	-- The only terms that affect it for monsters (that we know of) are MDB and MDT. If a normal monster
	-- would take 50k damage from your group, Botulus would take 40k damage. Every. Time. 
	local dmg = ((100+caster:getMod(MOD_MATT))/(100+target:getMod(MOD_MDEF))) * (caster:getStat(MOD_INT) + (caster:getMod(79+ELEMENTAL_MAGIC_SKILL)+caster:getSkillLevel(ELEMENTAL_MAGIC_SKILL))/6) * 3.5;

	--Add in target adjustment
	dmg = adjustForTarget(target,dmg,spell:getElement());
	-- Add in final adjustments
	dmg = finalMagicAdjustments(caster,target,spell,dmg);
	if (caster:getID() == 17297441) then -- For King Behemoth
		target:delHP(dmg*6);
		return dmg*6;
	else
		return dmg;
	end

end;