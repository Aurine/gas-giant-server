-----------------------------------------
-- Spell: Foe Requiem II
-- Deals sonic damage to enemies.
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local effect = EFFECT_REQUIEM;
	local duration = 79;
	local power = 2;

	local bonus = AffinityBonus(caster, spell:getElement());
	local pCHR = caster:getStat(MOD_CHR);
	local mCHR = target:getStat(MOD_CHR);
	local dCHR = (pCHR - mCHR);
	local resm = applyResistance(caster,spell,target,dCHR,SINGING_SKILL,bonus);
	if(resm < 0.5) then
		spell:setMsg(85);--resist message
		return 1;
	end

	local iBoost = caster:getMod(MOD_REQUIEM) + caster:getMod(MOD_ALL_SONGS);
	power = power + iBoost;

	if (caster:hasStatusEffect(EFFECT_SOUL_VOICE)) then
		power = power * 2;
	elseif (caster:hasStatusEffect(EFFECT_MARCATO)) then
		power = power * 1.5;
	end
	caster:delStatusEffect(EFFECT_MARCATO);

	duration = duration * ((iBoost * 0.1) + (caster:getMod(MOD_SONG_DURATION)/100) + 1);

	if (caster:hasStatusEffect(EFFECT_TROUBADOUR)) then
		duration = duration * 2;
	end
	-- Try to overwrite weaker slow / haste
	if(canOverwrite(target, effect, power)) then
		-- overwrite them
		target:delStatusEffect(effect);
		target:addStatusEffect(effect,power,3,duration);
		spell:setMsg(237);
	else
		spell:setMsg(75); -- no effect
	end

	return effect;
end;