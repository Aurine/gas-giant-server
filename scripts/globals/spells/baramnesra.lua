-----------------------------------------
-- Spell: Baramnesra
-- Increases resistance against amnesia
-- for party members within an area of
-- effect.
-- MP Cost: 60
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local enchanceSkill = caster:getSkillLevel(ENHANCING_MAGIC_SKILL);
	local duration = enchanceSkill * 2;
	if(duration > 480) then
		duration = 480;
	end
	local power = calculateBarspellPower(caster,enhanceSkill);

	if (caster:hasStatusEffect(EFFECT_COMPOSURE) == true and caster == target) then
		duration = duration * 3;
	end

	local extraBarspellEffect = 0;
	local body = caster:getEquipID(SLOT_BODY);

	if(caster:hasStatusEffect(EFFECT_AFFLATUS_SOLACE) == true) then
		if(body == 11186) then -- Orison Bliaud +1
			extraBarspellEffect = extraBarspellEffect + 5;
		elseif(body == 11086) then -- Orison Bliaud +2
			extraBarspellEffect = extraBarspellEffect + 10;
		end
	end

	target:delStatusEffect(EFFECT_BARAMNESIA);
	target:addStatusEffect(EFFECT_BARAMNESIA,power,0,duration,0,1,extraBarspellEffect);
end;