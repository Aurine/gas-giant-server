--------------------------------------
-- Spell: Luminohelix
-- Deals light damage that gradually
-- reduces a target's HP. Damage dealt
-- is greatly affected by the weather.
-- MP Cost: 26
--------------------------------------
 
require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	-- Get helix acc/att merits
	local merit = caster:getMerit(MERIT_HELIX_MAGIC_ACC_ATT);

	-- Calculate raw damage
	local dmg = calculateMagicDamage(35,1,caster,spell,target,ELEMENTAL_MAGIC_SKILL,MOD_INT,false);
	dmg = dmg + caster:getMod(MOD_HELIX_EFFECT);
	-- Get resist multiplier (1x if no resist)
	local resist = applyResistance(caster,spell,target,caster:getStat(MOD_INT)-target:getStat(MOD_INT),ELEMENTAL_MAGIC_SKILL,merit*3);
	-- Get the resisted damage
	dmg = dmg*resist;
	-- Add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
	dmg = addBonuses(caster,spell,target,dmg,merit*2);
	-- Add in target adjustment
	dmg = adjustForTarget(target,dmg,spell:getElement());
	-- Add in final adjustments
	dmg = finalMagicAdjustments(caster,target,spell,dmg);

	local duration = getHelixDuration(caster) + caster:getMod(MOD_HELIX_DURATION);

	duration = duration * (resist/2);

	target:addStatusEffect(EFFECT_HELIX,dmg,3,duration);

	return dmg;
end;