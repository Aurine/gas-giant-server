-----------------------------------------
-- Spell: Fire Threnody
-- Weakens the target enemy to Fire-based
-- attacks and effects.
-----------------------------------------
package.loaded["scripts/globals/magic"] = nil;
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	return handleThrenody(caster, target, spell, 50, 60, MOD_FIRERES);
end;