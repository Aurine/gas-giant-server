-----------------------------------------
-- Bluemagic: Zephyr Mantle
-- Creates shadow images that each absorb
-- a single attack directed at you.
-- AGI +2
-- Lvl.: 65 MP Cost: 31 Blue Points: 2
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)

	local duration = 300;

	if(caster:hasStatusEffect(EFFECT_DIFFUSION)) then
		local diffMerit = caster:getMerit(MERIT_DIFFUSION);

		if(diffMerit > 0) then
			duration = duration + (duration/100)* diffMerit;
		end

		caster:delStatusEffect(EFFECT_DIFFUSION);
	end

	if(target:addStatusEffect(EFFECT_BLINK, 4, 0, duration)) then
		spell:setMsg(230);
	else
		spell:setMsg(75);
	end

	return EFFECT_BLINK;
end;