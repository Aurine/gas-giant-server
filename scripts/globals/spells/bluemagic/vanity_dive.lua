-----------------------------------------
-- Bluemagic: Vanity Dive
-- Damage varies with TP.
-- AGI+3 CHR-2 
-- Lvl.: 82 MP Cost: 58 Blue Points: 2
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnMagicCastingCheck
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function onSpellCast(caster,target,spell)
	local params = {};
	-- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
	params.tpmod = TPMOD_DAMAGE; params.dmgtype = DMGTYPE_SLASH; params.scattr = SC_SCISSION;
	params.numhits = 1;
	params.multiplier = 3.00; params.tp150 = 4.00; params.tp300 = 4.50; params.azuretp = 2.875; params.duppercap = 75;
	params.str_wsc = 0.0; params.dex_wsc = 0.5; params.vit_wsc = 0.0; params.agi_wsc = 0.0; params.int_wsc = 0.0; params.mnd_wsc = 0.0; params.chr_wsc = 0.0;
	damage = BluePhysicalSpell(caster, target, spell, params);
	damage = BlueFinalAdjustments(caster, target, spell, damage, params);

	return damage;
end;