-----------------------------------------
-- Bluemagic: Actinic Burst
-- Greatly lowers the accuracy of enemies
-- within range for a brief period of
-- time.
-- HP +20 CHR +2
-- Lvl.: 74 MP Cost: 25 Blue Points: 4
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	-- Pull base stats.
	local dINT = (caster:getStat(MOD_MND) - target:getStat(MOD_MND));

	local resist = applyResistance(caster,spell,target,dINT,DIVINE_MAGIC_SKILL, 150);
	local duration = 20 * resist;

	if(resist > 0.0625) then
		if(target:addStatusEffect(EFFECT_FLASH,200,0,duration)) then
			spell:setMsg(236);
		else
			spell:setMsg(75);
		end
	else
		spell:setMsg(85);
	end
	return EFFECT_FLASH;
end;