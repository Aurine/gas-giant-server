-----------------------------------------
-- Bluemagic: Lowing
-- Gives enemies within range a powerful
-- disease that prevents recovery of HP
-- and MP.
-- HP -5
-- Lvl.: 71 MP Cost: 66 Blue Points: 2
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local duration = 60;
	
	local dINT = caster:getStat(MOD_MND) - target:getStat(MOD_MND);
	local resist = applyResistance(caster,spell,target,dINT,37);
	if(resist > 0.0625) then
		-- resisted!
		spell:setMsg(85);
		return 0;
	else
		target:addStatusEffect(EFFECT_PLAGUE,5,0,duration);
		spell:setMsg(236);
	end

	return EFFECT_PLAGUE;
	
end;