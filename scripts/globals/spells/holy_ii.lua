-----------------------------------------
-- Spell: Holy II
-- Deals light damage to an enemy.
-- MP Cost: 150
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	-- Calculate raw damage (holy 2 values from Foldypaws bluegartr @ Test server findings)
	local dmg = calculateMagicDamage(250,2,caster,spell,target,DIVINE_MAGIC_SKILL,MOD_MND,false);
	-- Get resist multiplier (1x if no resist)
	local resist = applyResistance(caster,spell,target,caster:getStat(MOD_MND)-target:getStat(MOD_MND),DIVINE_MAGIC_SKILL,1.0);
	-- Get the resisted damage
	dmg = dmg*resist;
	-- Add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
	dmg = addBonuses(caster,spell,target,dmg);
	-- Add in target adjustment
	dmg = adjustForTarget(target,dmg,spell:getElement());
	-- Add in final adjustments
	dmg = finalMagicAdjustments(caster,target,spell,dmg);

	if((caster:hasStatusEffect(EFFECT_DIVINE_EMBLEM) == true) and (target:isUndead() == true)) then
		if(target:hasStatusEffect(EFFECT_AMNESIA) == false) then
			target:addStatusEffect(EFFECT_AMNESIA,1,0,25);
		end
	end

	return dmg;
end;