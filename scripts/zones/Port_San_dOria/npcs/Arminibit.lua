-----------------------------------
-- Area: Port San d'Oria
-- NPC: Arminibit
-- Type: Standard Info NPC
-- Quests: Flyers for Regine
-- @zone 232
-- @pos -0.841 -8.000 -122.787
-----------------------------------
package.loaded["scripts/zones/Port_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/globals/teleports");
require("scripts/zones/Port_San_dOria/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	-- "Flyers for Regine" conditional script
	local FlyerForRegine = player:getQuestStatus(SANDORIA,FLYERS_FOR_REGINE);

	if(FlyerForRegine == 1) then
		local count = trade:getItemCount();
		local MagicFlyer = trade:hasItemQty(532,1);
		if(MagicFlyer == true and count == 1) then
			player:messageSpecial(FLYER_REFUSED);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:getMainLvl() >= 30 and player:getQuestStatus(SANDORIA,THE_HOLY_CREST) == QUEST_AVAILABLE) then
		player:startEvent(0x0018);
	else
		player:startEvent(0x024b);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);

	if(csid == 0x0018) then
		player:setVar("TheHolyCrest_Event",1);
		debugTeleport(player,17461503); -- Novalmauge
	end

end;