-----------------------------------
-- Area: Port Jeuno
-- NPC: Najib
-- Type: Standard Info NPC
-- @zone 246
-- @pos -6.433 10.999 -76.511
-----------------------------------
package.loaded["scripts/zones/Port_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Port_Jeuno/TextIDs");
require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	if (TRAVEL_SKIP >= 1) then
		if (trade:getGil() >= TRAVEL_SKIP and trade:getItemCount() == 1) then
			player:delGil(TRAVEL_SKIP);
			player:setPos(222,-6,92,164,240);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x272a);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;