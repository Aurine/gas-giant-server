-----------------------------------
-- Area: Xarcabard (112)
-- Mob: Shadow Eye
-- Quests: The Miraculous Dale
-- @zone 112
-- @pos -238.000, -12.000, 83.000
-----------------------------------

-- require("scripts/zones/Xarcabard/MobIDs");
require("scripts/globals/quests");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)	
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)	
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)	
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)	
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",11,true);
	end
end;

