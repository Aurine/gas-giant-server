-----------------------------------
-- Area: Bastok Markets
-- NPC: Raghd
-- Type: Standard Merchant NPC
-- @zone 235
-- @pos -149.200 -4.819 -74.939
-----------------------------------
package.loaded["scripts/zones/Bastok_Markets/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/shop");
require("scripts/zones/Bastok_Markets/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    player:showText(npc,RAGHD_SHOP_DIALOG);

    local stock = {
        0x3490,  1125,1,     --Silver Ring
        0x340F,  1125,1,     --Silver Earring

        0x3499,   180,2,     --Brass Ring

        0x348E,    68,3      --Copper Ring
    }
    showNationShop(player, BASTOK, stock);

end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;