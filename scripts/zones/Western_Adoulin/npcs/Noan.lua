-----------------------------------
-- Area: Western Adoulin
-- NPC: Noan
-- Type: Patrol NPC
-- @zone 256
-- @pos 23.697 0.001 -84.416
-----------------------------------
package.loaded["scripts/zones/Western_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Western_Adoulin/TextIDs");
require("scripts/globals/pathfind");

-----------------------------------

local path = {
23.696999,0.000004,-109.855919,
23.696980,-0.067329,-108.751877,
23.696993,0.000004,-78.622925,
23.696995,0.000004,-58.952606,
23.696995,0.000000,-88.957718
};

-----------------------------------
-- onSpawn Action
-----------------------------------

function onSpawn(npc)
	npc:initNpcAi();
	npc:setPos(pathfind.first(path));
	onPath(npc);
end;

-----------------------------------
-- onPath Action
-----------------------------------

function onPath(npc)
	pathfind.patrol(npc, path);
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0220);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;