-----------------------------------
-- Area: Western Adoulin
-- NPC: Safrick
-- Type: Patrol NPC
-- @zone 256
-- @pos 26.009 0.001 69.575
-----------------------------------
package.loaded["scripts/zones/Western_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Western_Adoulin/TextIDs");
require("scripts/globals/pathfind");

-----------------------------------

local path = {
26.008987,0.000000,70.573807,
26.008972,0.000000,69.469772,
26.008989,-0.150000,42.001362
};

-----------------------------------
-- onSpawn Action
-----------------------------------

function onSpawn(npc)
	npc:initNpcAi();
	npc:setPos(pathfind.first(path));
	onPath(npc);
end;

-----------------------------------
-- onPath Action
-----------------------------------

function onPath(npc)
	pathfind.patrol(npc, path);
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0227);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;