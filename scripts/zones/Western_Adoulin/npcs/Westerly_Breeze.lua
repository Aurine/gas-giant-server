-----------------------------------
-- Area: Western Adoulin
-- NPC: Westerly Breeze
-- Type: Patrol NPC
-- @zone 256
-- @pos 62.500 31.999 123.000
-----------------------------------
package.loaded["scripts/zones/Western_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Western_Adoulin/TextIDs");
require("scripts/globals/pathfind");

-----------------------------------

local path = {
 61.575569,32.000000,122.943474,
 60.961201,32.000000,123.865509,
 60.337997,32.000000,124.762199,
 59.680527,32.000000,125.643944,
 59.004333,32.000000,126.435379,
 58.299999,32.000000,127.178093,
 55.916615,32.000000,129.581390,
 56.925484,32.000000,128.573059,
 58.612652,32.000000,126.885994,
 59.322113,32.000000,126.176712,
 61.293381,32.000000,124.205612,
 61.929314,32.000000,123.417976,
 61.636917,32.000000,122.446983,
 61.036896,32.000000,121.640205,
 60.352127,32.000000,120.878044,
 59.644192,32.000000,120.139839,
 39.299885,32.000000,99.654045,
 60.470589,32.000000,120.957817
};

-----------------------------------
-- onSpawn Action
-----------------------------------

function onSpawn(npc)
	npc:initNpcAi();
	npc:setPos(pathfind.first(path));
	onPath(npc);
end;

-----------------------------------
-- onPath Action
-----------------------------------

function onPath(npc)
	pathfind.patrol(npc, path);
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0229);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;