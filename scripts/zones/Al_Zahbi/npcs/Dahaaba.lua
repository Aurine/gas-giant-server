-----------------------------------
-- Area: Al Zahbi
-- NPC: Dahaaba
-- Type: Chocobo Renter
-- @zone 48
-- @pos -65.309, -1, -39.585
-----------------------------------
package.loaded["scripts/zones/Al_Zahbi/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/zones/Al_Zahbi/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();
	local count = trade:getItemCount();
	local ChocoboTicket = trade:hasItemQty(1514,1);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;

	if(ChocoboTicket == true and count == 1) then
		if (hasLicense and level >= 20) then
			player:tradeComplete();
			player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			player:setPos(610,-24,356,0x80,0x33);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	
	local price = 100;
	local gil = player:getGil();

	if(player:hasKeyItem(CHOCOBO_LICENSE) and player:getMainLvl() >= 20) then
		player:startEvent(0x010e,price,gil);
	else
		player:startEvent(0x010ef,price,gil);
	end
	
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;	
	local price = 100;
	local level = player:getMainLvl();

	if(csid == 0x010e and option == 0) then
		if(player:delGil(price)) then
			if(level >= 20) then
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			else
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(900 + durationMod),true);
			end
			player:setPos(610,-24,356,0x80,0x33);
		end
	end	
end;