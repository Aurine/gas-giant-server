-----------------------------------
-- Area: Ra'Kaznar Inner Court
-- NPC: Vertical Transit Device
-- Zones to Outer Ra'Kaznar (zone 274)
-- @zone 276
-- @pos -494 -522 20
-----------------------------------
package.loaded["scripts/zones/RaKaznar_Inner_Court/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/RaKaznar_Inner_Court/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    -- player:startEvent( ? );
    player:setPos(-408.5,-140,-20,0,274);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
    -- if (csid == ? and option == 1) then
        -- player:setPos(-476,-520.5,20,0,276);
    -- end
end;