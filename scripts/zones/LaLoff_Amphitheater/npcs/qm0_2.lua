-------------------------------------
-- Area: La'Loff Amphitheater
-- NPC: (???) qm0
-- @zone 180
-- @pos -302.024 -95.430 -424.504
-- Notes: warp the player outside
-- after they win fight
-------------------------------------
package.loaded["scripts/zones/LaLoff_Amphitheater/TextIDs"] = nil;
-------------------------------------

require("scripts/zones/LaLoff_Amphitheater/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    print(npc:getID());

	player:startEvent(0x0C);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);	
    if (csid == 0x0C and option == 1) then
        player:setPos(0.049,-42.088,469.093,74,130);
    end
end;