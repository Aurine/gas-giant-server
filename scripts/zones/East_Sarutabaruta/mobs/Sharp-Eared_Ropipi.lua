-----------------------------------
-- Area: East Sarutabaruta (116)
-- NM: Sharp-Eared_Ropipi
-- Quests: The Miraculous Dale
-- @zone 116
-- @pos 332.000, -13.000, -325.000
-----------------------------------

require("scripts/globals/quests");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Sharp_Eared_Ropipi's Window Open Time
	SetServerVariable("[POP]Sharp_Eared_Ropipi", os.time(t) + 300); -- 5 minutes
	DeterMob(mob:getID(), true);
    
	-- Set PH back to normal, then set to respawn spawn
	PH = GetServerVariable("[PH]Sharp_Eared_Ropipi");
	SetServerVariable("[PH]Sharp_Eared_Ropipi", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));
	
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",12,true);
	end
  
end;

