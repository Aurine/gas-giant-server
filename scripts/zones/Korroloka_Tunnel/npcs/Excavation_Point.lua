-----------------------------------
-- Area: Korroloka Tunnel
-- NPC: Excavation Point
-- @zone 173
-- @pos -162.855 -7.433 135.520
-- @pos 146.129 -0.971 -82.968
-- @pos 61.417 0.342 -29.337
-- @pos -102.172 -1.953 -30.428
-- @pos 61.417 0.342 -29.337
-- @pos -146.129 -0.971 -82.968
-----------------------------------
package.loaded["scripts/zones/Korroloka_Tunnel/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/excavation");
require("scripts/zones/Korroloka_Tunnel/TextIDs");

-----------------------------------
-- onTrade
-----------------------------------

function onTrade(player,npc,trade)
	startExcavation(player,player:getZone(),npc,trade,0x0000);
end;

-----------------------------------
-- onTrigger
-----------------------------------

function onTrigger(player,npc)
	player:messageSpecial(MINING_IS_POSSIBLE_HERE,605);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;