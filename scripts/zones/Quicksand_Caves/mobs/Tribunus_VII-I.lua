-----------------------------------	
-- Area: Quicksand Caves
-- NM: Tribunus VII-I
-- @zone 208
-- @pos -47.410 -0.312 -139.298
-- Note: Spawned by trading an
-- Antican Tag to the ??? at (D-9)
-- of the third map.
-----------------------------------	

-----------------------------------
-- onMobFight Action
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	GetNPCByID(17629658):hideNPC(900); -- qm2 in npc_list
end;
