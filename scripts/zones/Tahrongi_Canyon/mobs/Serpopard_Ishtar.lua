-----------------------------------
-- Area: Tahrongi Canyon
-- NM: Serpopard Ishtar
-- @zone 117
-- @pos 49.540, 0.843, -61.629
-----------------------------------

-- require("scripts/zones/Tahrongi_Canyon/MobIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)	
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)	
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)	
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)	
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	if (killer:getVar("Windy_Tutorial_NPC") == 8) then
		killer:setVar("Windy_Tutorial_NPC",9);
	end
end;