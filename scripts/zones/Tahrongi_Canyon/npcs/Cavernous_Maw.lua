-----------------------------------
-- Area: Tahrongi Canyon
-- Name: Cavernous Maw
-- Teleports Players to Abyssea
-- Tahrongi
-- @zone 117
-- @pos -28.597, 46.056, -685.754
-----------------------------------
package.loaded["scripts/zones/Tahrongi_Canyon/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/teleports");
require("scripts/globals/missions");
require("scripts/zones/Tahrongi_Canyon/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (player:getMainLvl() >= ABYSSEA_MIN_LV) then
		player:startEvent(0x0064,1,1,1,1,1,1,1);
	else
		player:messageSpecial(NOTHING_HAPPENS);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if(csid == 0x0064 and option == 1) then
		player:setPos(-24,44,-678,240,45);
	end
end;