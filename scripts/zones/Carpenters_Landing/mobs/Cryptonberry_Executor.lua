-----------------------------------
-- Area: Carpenters' Landing
-- NM: Cryptonberry Executor
-- @zone 2
-- @pos 123.000 -5.000 -408.000
-----------------------------------

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobFight Action
-----------------------------------

function onMobFight(mob,target)	
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == CALM_BEFORE_THE_STORM and killer:getVar("Cryptonberry_Executor_KILL") == 0)then
		killer:setVar("Cryptonberry_Executor_KILL",1);
	end
end;
