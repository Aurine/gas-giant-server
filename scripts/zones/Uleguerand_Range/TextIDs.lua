-- Area: Uleguerand Range
-- Last updated: client ver. 30140910_0

-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 6391; -- You cannot obtain the item <item>. Come back after sorting your inventory.
          ITEM_OBTAINED = 6394; -- Obtained: <item>.
           GIL_OBTAINED = 6395; -- Obtained <number> gil.
       KEYITEM_OBTAINED = 6397; -- Obtained key item: <keyitem>.

-- Other dialog
NOTHING_OUT_OF_ORDINARY = 6408; -- There is nothing out of the ordinary here.
        NOTHING_HAPPENS =  119; -- Nothing happens...

-- conquest Base
CONQUEST_BASE = 7037;

-- Other Texts
-- NOTHING_OUT_OF_ORDINARY = 7197; -- There is nothing out of the ordinary here.
-- NOTHING_OUT_OF_ORDINARY = 7227; -- There is nothing out of the ordinary here.
        -- NOTHING_HAPPENS = 7246; -- Nothing happens.
        -- NOTHING_HAPPENS = 7262; -- Nothing happens...
      MYSTIC_ICE_OFFSET = 7208; -- You see something glittering beneath the surface of the ice.
