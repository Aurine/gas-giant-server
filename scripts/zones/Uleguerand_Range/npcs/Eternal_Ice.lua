-----------------------------------
-- Area: Uleguerand Range
-- NPC:  Eternal Ice
-- Type: Item NPC
-- @zone 5
-- @pos 575.536 -26.067 -101.472
-- @pos 455.518 -82.107 421.435
-- @pos -95.498 -146.098 378.526
-- Note: Gives key item
-- "Mystic Ice" upon examining.
-----------------------------------
package.loaded["scripts/zones/Uleguerand_Range/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/zones/Uleguerand_Range/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    if (player:hasKeyItem(MYSTIC_ICE) == false) then
        player:addKeyItem(MYSTIC_ICE);
        player:messageSpecial(KEYITEM_OBTAINED,MYSTIC_ICE);
    else
        player:messageSpecial(NOTHING_OUT_OF_ORDINARY);
    end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;