-----------------------------------
-- Area: Valkurm Dunes
-- MOB:  Doman
-- Involved in Quest: Yomi Okuri
-- @zone 103
-- @pos -768.000, -4.000, 197.000
-----------------------------------

require("scripts/globals/keyitems");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	if(killer:hasKeyItem(YOMOTSU_HIRASAKA)) then
		killer:setVar("OkuriNMKilled",killer:getVar("OkuriNMKilled") + 1);
	end

end;