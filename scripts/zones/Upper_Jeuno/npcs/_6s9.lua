-----------------------------------
-- Area: Upper Jeuno
-- Door: Goddess Temple
-- @zone 244
-- @pos -35.144, -3.573, 23.110
-- cutscenes 0x009f  0x2723  0x274c  0x27dd  0x274f  0x2750  0x2751  0x2749  0x2758  0x2759  0x275a  0x275b
-- 0x27bf  0x27d0  0x27d1  0x27d2  0x27d3  0x27d4  0x27d5  0x27d6  0x27d7  0x27d8  0x27d9  0x27da  0x27db  0x27e8
-----------------------------------
package.loaded["scripts/zones/Upper_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Upper_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	npc:openDoor();
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;