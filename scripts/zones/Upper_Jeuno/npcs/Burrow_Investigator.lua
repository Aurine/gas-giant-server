-----------------------------------
-- Area: Upper Jeuno
-- NPC: Burrow Investigator
-- Type: Quest NPC
-- @zone 244
-- @pos 7.675, 0.200, 73.105
--
-- Menu
-- player:startEvent(0x157C,expeditions completed ?,?,?,?,research marks,?,player:getGil());
-----------------------------------
package.loaded["scripts/zones/Upper_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Upper_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getVar("BurrowExplanation") == 0) then -- Initial CS tracking
		player:startEvent(0x157F); -- first diagloge explanation
	else
		player:startEvent(0x157C,10,2,3,4,5000,6,player:getGil());
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
printf("CSID: %u",csid);
printf("RESULT: %u",option);
	if(option == 3) then
		player:updateEvent(1000,2000,3000,4000,5000,6000,player:getGil(),8000); -- Update
	elseif(option == 15) then
		player:updateEvent(1,2,3,4,5,6,7,8); -- Update
	end
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
printf("CSID: %u",csid);
printf("RESULT: %u",option);
	if (csid == 0x157F) then
		player:setVar("BurrowExplanation",1); -- set initial CS viewed
	elseif(csid == 0x157C and option == 65538) then
		player:setPos(283,24,118,89,120);
	elseif(csid == 0x157C and option == 131074) then
		player:setPos(283,8,-117,88,105);
	end
end;