-----------------------------------
-- Area: Upper Jeuno
-- Door: House
-- @zone 244
-- @pos -66.357, -2.750, 60.515
-- cutscenes 0x00c0  0x00c5  0x00c6  0x2783  0x2786  0x2791  0x2793  0x2795  0x279b  0x27e7  0x27a7  0x27e1  0x27e2  0x27e3  0x27bc
-----------------------------------
package.loaded["scripts/zones/Upper_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Upper_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	npc:openDoor();
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;