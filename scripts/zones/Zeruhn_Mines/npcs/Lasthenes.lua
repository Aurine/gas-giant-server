-----------------------------------
-- Area: Zeruhn Mines
-- NPC: Lasthenes
-- @zone 172
-- @pos -78.366 0.000 21.864
-- Notes: Opens Gate
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (player:getXPos() > -79.5 ) then
		player:startEvent(0x00B4);
	else
		player:startEvent(0x00B5);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;