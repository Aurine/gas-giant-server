-----------------------------------
-- Area: Windurst Woods
-- NPC: Auction Counter
-- Quest: Tutorial NPC
-- @zone 241
-- @pos many
-----------------------------------

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (player:getVar("Windy_Tutorial_NPC") == 4) then
		player:setVar("Windy_Tutorial_NPC",5);
	end
	player:sendMenu(3);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
