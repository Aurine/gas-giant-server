-----------------------------------
-- Area: Windurst Woods
-- NPC:  Sola Jaab
-- Type: Quest NPC
-- Quests: Riding on the Clouds
-- @zone 241
-- @pos 109.383 -4.999 -25.925
-----------------------------------
package.loaded["scripts/zones/Windurst_Woods/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/Windurst_Woods/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(player:getQuestStatus(JEUNO,RIDING_ON_THE_CLOUDS) == QUEST_ACCEPTED and player:getVar("ridingOnTheClouds_4") == 7) then
		if(trade:hasItemQty(1127,1) and trade:getItemCount() == 1) then -- Trade Kindred seal
			player:setVar("ridingOnTheClouds_4",0);
			player:tradeComplete();
			player:addKeyItem(SPIRITED_STONE);
			player:messageSpecial(KEYITEM_OBTAINED,SPIRITED_STONE);
		end
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (player:getRace() == 7) then
		player:startEvent(0x006E);
	else
		player:startEvent(0x006D);

	-- All based on what mission for Windurst you are on.
		-- player:startEvent(0x0075);
		-- player:startEvent(0x007D); -- Horutoto Ruins
		-- player:startEvent(0x0082); -- Minister run in
		-- player:startEvent(0x0086); -- Cardians
		-- player:startEvent(0x0099); -- yagudo pact
		-- player:startEvent(0x009E); -- rank 2
		-- player:startEvent(0x00A2); -- library books
		-- player:startEvent(0x01DB); -- Tarutaru children study school of magic
		-- player:startEvent(0x00B4); -- great windurst is
		-- player:startEvent(0x00B8); -- Semih Lafihna
		-- player:startEvent(0x00BE); -- ladies in waiting
		-- player:startEvent(0x00C3); -- piece of paper
		-- player:startEvent(0x00C8); -- central tower horutoto ruins
		-- player:startEvent(0x00D0); -- talisman
		-- player:startEvent(0x00D5); -- warefare avoided
		-- player:startEvent(0x00D8); -- warefare avoided
		-- player:startEvent(0x0253); -- Captian Rakoh Buuma
		-- player:startEvent(0x0230); -- going easy here
		-- player:startEvent(0x0235); -- another incident
		-- player:startEvent(0x023B); -- rank 7
		-- player:startEvent(0x0240); -- mithra come from far south
		-- player:startEvent(0x0245); -- 50 points
		-- player:startEvent(0x0263); -- Meow!
		-- player:startEvent(0x024a); -- guards in on secret
		-- player:startEvent(0x0268); -- tarutaru vicious
		-- player:startEvent(0x0276); -- grrr upset
		-- player:startEvent(0x0273); -- work dry up
		-- player:startEvent(0x027A); -- learned your name
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;