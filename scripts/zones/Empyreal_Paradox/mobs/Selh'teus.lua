-----------------------------------
-- Area: Empyreal Paradox
--  NPC: Selh'teus
-- Chains of Promathia 8-4 BCNM Fight
-----------------------------------
package.loaded["scripts/zones/Empyreal_Paradox/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");
require("scripts/zones/Empyreal_Paradox/TextIDs");

-----------------------------------
-- onMobInitialize Action
-----------------------------------

function onMobInitialize(mob)
    mob:addMod(MOD_REGAIN, 50);
    mob:SetAutoAttackEnabled(false);
end

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged Action
-----------------------------------

function onMobEngaged(mob, target)
end;

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobFight(mob, target)
    if (target:getTarget():getID() ~= mob:getID()) then
        local targetPos = target:getPos();
        local radians = (256 - targetPos.rot) * (math.pi / 128);
        mob:pathTo(targetPos.x + math.cos(radians) * 16, targetPos.y, targetPos.z + math.sin(radians) * 16);
    end
    local lanceTime, lanceOut, rejuv = mob:getExtraVar(3);
    if (mob:getHPP() < 30 and rejuv == 0 and target:getFamily() == 478) then
        mob:messageText(mob, SELHTEUS_TEXT + 2);
        mob:useMobAbility(1253);
        mob:setExtraVar(lanceTime, lanceOut, 1);
    elseif lanceTime + 50 < mob:getBattleTime() and lanceOut == 0 then
        mob:entityAnimationPacket("sp00");
        mob:setExtraVar(lanceTime, 1, rejuv);
    end
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
    mob:messageText(mob, SELHTEUS_TEXT);
    mob:getBattlefield():lose();
end;