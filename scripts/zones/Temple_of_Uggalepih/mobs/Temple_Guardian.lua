-----------------------------------
-- Area: Temple of Uggalepih
-- NPC:  Temple Guardian
-- @zone 159
-- @pos -61.734, -0.207, -99.821
-----------------------------------

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	GetNPCByID(17428956):openDoor(300); -- 5min
end;