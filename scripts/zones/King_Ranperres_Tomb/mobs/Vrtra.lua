-----------------------------------
-- Area: King Ranperre's Tomb
--  HNM: Vrtra
-- @zone 190
-- @pos 228.000 7.134 -311.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/titles");

-----------------------------------
-- OnMobInitialize Action
-----------------------------------

function onMobInitialize(mob)
    mob:addMod(MOD_REGEN, 30);
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
    killer:addTitle(VRTRA_VANQUISHER);

    -- Set Vrtra's spawnpoint and respawn time (3-5 days)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime((math.random((259200),(432000))) /HNM_TIMER_MOD);

end;