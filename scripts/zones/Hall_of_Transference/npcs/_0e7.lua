-----------------------------------
-- Area: Hall of Transference - Mea
-- Large Apparatus (Left) - Mea
-- @zone 14
-- @pos 270.000 -84.900 -36.299
-- PH PM PD
-- 1=available 2=working
-- 3=complete
-----------------------------------
package.loaded["scripts/zones/Hall_of_Transference/TextIDs"] = nil;
-----------------------------------
require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("/scripts/globals/missions");
require("scripts/globals/titles");
require("/scripts/globals/teleports");
require("scripts/zones/Hall_of_Transference/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	
	if(player:getVar("PromathiaStatus") == 2)then 
		player:startEvent(0x00A0);
	elseif(player:getVar("PM") == 2)then
		player:startEvent(0x0080);
	elseif(player:getVar("TuLiaRegistration") == 1)then
		player:messageSpecial(REGISTERED_DATA);
	else
		player:messageSpecial(NODATA);
		player:messageSpecial(REGISTER_DATA);
	end
	return 1;
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);

	if(csid == 0x00A0) then
		if(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
			player:setPos(-107 ,0 ,223 ,164 ,20 ); -- Teleport to Promyvion Mea
		else
			player:setVar("PM",2);
			player:setPos(-107 ,0 ,223 ,164 ,20 ); -- Teleport to Promyvion Mea
		end
	elseif(csid == 0x0080 and option == 0)then 
		player:setVar("PM",1);
		player:setPos(179, 35, 257, 195, 117);
	end
end;