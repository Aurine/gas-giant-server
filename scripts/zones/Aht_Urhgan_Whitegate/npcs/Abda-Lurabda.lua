-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Abda Lurabda
-- Type: Puppet Name Changer
-- Missions: ToAU 9
-- @zone 50
-- @pos 37.313 -7.800 51.351
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/status");
require("scripts/globals/pets");
require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    if (player:getMainJob() == JOB_PUP) then
        player:startEvent(0x288, 0, 9800, player:getGil());
    else
        player:startEvent(0x101);
    end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %d",option);
    if (csid == 0x288 and bit.band(option, 0x80000000) ~= 0) then
        player:delGil(9800);
        local page = bit.band(option, 0xF);
        local val = bit.rshift(bit.band(option, 0xFFFFF0), 4);
        player:setPetName(PETTYPE_AUTOMATON,86 + val + page*32);
        player:messageSpecial(5747);
    end
end;