-- Area: Residential Area
-- Last updated: client ver. 30140910_0

-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 0; -- You cannot obtain the item <item> come back again after sorting your inventory
          ITEM_OBTAINED = 0; -- Obtained: <item>
           GIL_OBTAINED = 0; -- Obtained <number> gil
       KEYITEM_OBTAINED = 0; -- Obtained key item: <keyitem>
