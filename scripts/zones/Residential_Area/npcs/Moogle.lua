-----------------------------------
-- Area: Residential Area
-- NPC: Moogle
-- Quests: Starlight Celebration,
-- Give a Moogle a Break,
-- Unexpected Treasure, The Moogles
-- Picnic, Moogles in the Wild
-----------------------------------
-- Cutscenes (Tested in Bastok Markets Mog House)
-- 0x7530 - Mog House explanation
-- 0x7532 - Repeat the Mog House explanation
-- 0x7531 - I traveled all the way here at your bidding, kupo! Even a Rent-a-Room needs a moogle, kupo!
-- 0x7533 - Master! I have something to tell you, kupo!
-- 0x7534 - For your own safety ... (Home Point prompt)
-- 0x7535 - Give your moogle a vacation? (Menu prompt)
-- 0x7536 - Papa Moogle will be entering the archery contest, so I want to bring him <Possible Special Code: 01><Possible Special Code: 01><Possible Special Code: 01≻ ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｅ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ as a gift, kupo!<Prompt>
--			Mama Moogle always likes to dress up for the occasion, and ≺Possible Special Code: 01≻≺Possible Special Code: 01≻≺Possible Special Code: 01> ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｆ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ would make the perfect present, kupo!<Prompt>
--			But I have no idea how to go about finding <Possible Special Code: 01><Possible Special Code: 01><Possible Special Code: 01> <Possible Special Code: 01><Possible Special Code: 05≻$ｅ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ or ≺Possible Special Code: 01≻≺Possible Special Code: 01><Possible Special Code: 01> <Possible Special Code: 01><Possible Special Code: 05≻$ｆ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo... I can't go home empty-handed, kupo...<Prompt>
-- 0x7537 - Master I'm overwhelmed by your kindness, kupo!! I'll bring you a nice present from the moogles' kometown kupo! I'll be back in a few days, kupo!
-- 0x7538 - Master I'm back from my vacation, kupo! Here's your present--a shiny new Mog Safe, kupo! Now you can store as many as 60 items, kupo!
-- 0x7539 - Give your moogle a vacation? (Menu prompt)
-- 0x753A - Papa Moogle is mad for fishing, so I want to give him ≺Possible Special Code: 01≻≺Possible Special Code: 01≻≺Possible Special Code: 01≻ ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｅ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo!≺Prompt≻
--			Mama Moogle would bake a scrumptious mooglepie to take with us, but she'd need ≺Possible Special Code: 01≻≺Possible Special Code: 01≻≺Possible Special Code: 01≻ ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｆ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo!≺Prompt≻
--			But...I haven't the foggiest idea of how to get either of these things, kupo... Whatever am I to do, kupo?<Prompt>
-- 0x753B - Master! I'm overwhelmed by your kindness, kupo!!!<Prompt> I'll bring you a nice present from the moogles' hometown, kupo! I'll be back in a few days, kupo!<Prompt>
-- 0x753C - Master! I'm back from my vacation, kupo!<Prompt> Here's your present--the behemoth of Mog Safes, kupo! Now you can store as many as 70 items, kupo!<Prompt>
-- 0x753D - Master, Master! I have a favor to ask, kupo!<Prompt> After resting on the ≺Possible Special Code: 01≻≺Possible Special Code: 05≻#ｃ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, I noticed a letter had arrived from some old moogle friends, kupo! Will my magnanimous master give me some time off to go visit my old buddies?<Prompt>
-- 0x753E - The sun beats down mercilessly in the land beyond the west sea, so I need to take <Possible Special Code: 01≻≺Possible Special Code: 01≻≺Possible Special Code: 01> ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｅ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ for my friends there, kupo!<Prompt>
--			And my other buddies need <Possible Special Code: 01><Possible Special Code: 01≻≺Possible Special Code: 01≻ ≺Possible Special Code: 01≻≺Possible Special Code: 05≻$ｆ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ for the torrential rains that scour the lands across the east sea, kupo!<Prompt>
--			I can't help out my friends if I don't have <Possible Special Code: 01><Possible Special Code: 01><Possible Special Code: 01> <Possible Special Code: 01><Possible Special Code: 05≻$ｅ≺BAD CHAR: 80><BAD CHAR: 80> and <Possible Special Code: 01><Possible Special Code: 01><Possible Special Code: 01> <Possible Special Code: 01><Possible Special Code: 05≻$ｆ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo...≺Prompt≻
-- 0x753F - I'll be sure to bring you a souvenir from my overseas trip, kupo! See you in a few days, master!<Prompt>
-- 0x7540 - Master I'm back from visiting my old moogle friends, kupo! We all got together and made you a new Mog Safe, kupo! Now you can store as many as 80 items, kupo!
-- 0x7541 - Master! Master! Look what I found underneath the ≺Possible Special Code: 01≻≺Possible Special Code: 05≻#ｃ≺BAD CHAR: 80≻≺BAD CHAR: 80≻ this morning, kupo!≺Prompt≻
--			It has been wrapped and everything! I wonder who it could be from, kupo!<Prompt>
-- 0x7542 - Master, Master! I found this in the ≺Possible Special Code: 01≻≺Possible Special Code: 05≻#ｃ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo!≺Prompt≻
--			It must be a present from the smilebringers. Here, you should have it, kupo!<Prompt>
-- 0x7543 - Master, Master! I found this in the ≺Possible Special Code: 01≻≺Possible Special Code: 05≻#ｃ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo!≺Prompt≻
--			It must be a present from the smilebringers. Here, you should have it, kupo!<Prompt>
-- 0x7544 - Master, Master! I found this in the ≺Possible Special Code: 01≻≺Possible Special Code: 05≻#ｃ≺BAD CHAR: 80≻≺BAD CHAR: 80≻, kupo!≺Prompt≻
--			It must be a present from the smilebringers. Here, you should have it, kupo!<Prompt>
-- 0x7545 - Master! What <Multiple Choice (Parameter 7)>[an egg-ceptional specimen/egg-ceptional specimens] you have there! Now, if you'll just let me have a look-see, kupo...<Prompt>
--			We egg-tach this thingamajig to that thingamabob, just like so, kupo...<Prompt> Now for the coup d'egg-grace...!<Prompt>
--			Voila! Isn't she a beauty, kupo? Utterly egg-squisite workmanship, I do degg-clare!<Prompt>
-- 0x7546 - Master! What <Multiple Choice (Parameter 7)>[an egg-ceptional specimen/egg-ceptional specimens] you have there! Now, if you'll just let me have a look-see, kupo...<Prompt>
--			We egg-stract this doodad from that doohickey, just like so, kupo...<Prompt>
--			Voila! Your original egg-redients, entirely egg-tact, kupo! If you change your mind, just trade them back to me or one of my moogle egg-sociates!<Prompt>
-- 0x7547 - It all began with a raindrop or so the legend says ... (Huge cutscene)
-- 0x7548 - Little did anyone suspect that this ... (Huge cutscene)
-- 0x7549 - It all began with a raindrop or so the legend says ... (warps you somewhere at least changes your room lol crashes when you @zone)
-----------------------------------

require("scripts/globals/moglocker")

-----------------------------------
-- getMogLockerTextOffset Action
--
-- Get the offset of:
-- Your Mog Locker lease is valid
-- until XXXXX, kupo.<Prompt>
-- for a given zone.
-----------------------------------

function getMogLockerTextOffset(zone)
	if (zone == 50) then -- Whitegate
		return 1160;
	elseif (zone == 48) then -- Al Zahbi
		return 7318;
	elseif (zone == 245) then -- Lower Jeuno
		return 6706;
	elseif (zone == 244) then -- Upper Jeuno
		return 6689;
	elseif (zone == 246) then -- Port Jeuno
		return 6674;
	elseif (zone == 243) then -- Ru'Lude Gardens
		return 6635;
	elseif (zone == 230) then -- Southern San d'Oria
		return 6572;
	elseif (zone == 231) then -- Northern San d'Oria
		return 6730;
	end
	return 6600;
end

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local numBronze = trade:getItemQty(2184);
	if (numBronze > 0) then
		if (addMogLockerExpiryTime(player, numBronze)) then
			-- Remove bronze
			player:tradeComplete();
			-- Send event
			player:messageSpecial(getMogLockerTextOffset(player:getPreviousZone()) + 2,getMogLockerExpiryTimestamp(player));
		end
	end
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	-- player:PrintToPlayer( string.format( " %i ", npc:getID() ) );

	local lockerTs = getMogLockerExpiryTimestamp(player);
	if (lockerTs ~= nil) then
		if (lockerTs == -1) then -- expired
			player:messageSpecial(getMogLockerTextOffset(player:getPreviousZone()) + 1,2184); -- 2184 is imperial bronze piece item id
		else
			player:messageSpecial(getMogLockerTextOffset(player:getPreviousZone()),lockerTs);
		end
	end

	local loadintro = player:getVar("MoghouseExplication");
	if (loadintro == 1) then
		player:startEvent(0x7530);
	else
		player:sendMenu(1);
	end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x7530) then
		player:setVar("MoghouseExplication",0);
	end
end;