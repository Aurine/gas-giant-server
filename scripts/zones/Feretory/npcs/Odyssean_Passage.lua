-----------------------------------
-- Area: Feretory
-- NPC: Odyssean Passage
-- @zone 285
-- @pos -358.000 -3.150 -470.000
-----------------------------------
package.loaded["scripts/zones/Feretory/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Feretory/TextIDs");

-----------------------------------
-- onTrade
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger
-----------------------------------

function onTrigger(player,npc,prevZone)
	-- This is temporary
	if(prevZone == 231) then
		player:setPos(0,0,0,0,231);
	elseif(prevZone == 236) then
		player:setPos(0,0,0,0,236);
	elseif(prevZone == 240) then
		player:setPos(0,0,0,0,240);
	elseif(prevZone == 109) then
		player:setPos(0,0,0,0,109);
	else
		-- Error, no prevZone to get so go to Lower Jeuno
		player:setPos(0,0,0,0,245);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;