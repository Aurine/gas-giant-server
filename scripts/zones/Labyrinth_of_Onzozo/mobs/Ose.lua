----------------------------------	
-- Area: Labyrinth of Onzozo
-- NM: Ose
-- @zone 213
-- @pos 17.000 4.000 152.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	
  
	-- Set Ose's Window Open Time
	SetServerVariable("[POP]Ose", os.time(t) + (3600 /NM_TIMER_MOD)); -- 1 hour
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Ose");
	SetServerVariable("[PH]Ose", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;