----------------------------------	
-- Area: Labyrinth of Onzozo
-- NM: Peg Powler
-- @zone 213
-- @pos -77.000 4.000 58.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	
  
	-- Set Peg Powler's Window Open Time
    local wait = math.random((7200),(57600));
	SetServerVariable("[POP]Peg_Powler", os.time(t) + (wait /NM_TIMER_MOD)); -- 2-16 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Peg_Powler");
	SetServerVariable("[PH]Peg_Powler", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;