-----------------------------------
-- Area: Cloister of Storms
-- NPC: Lightning Protocrystal
-- Quests: Trial by Lightning,
-- Trial Size Trial by Lightning
-- @zone 202
-- @pos 535.088 -14.931 493.012
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Storms/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-------------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/globals/quests");
require("scripts/globals/missions");
require("scripts/zones/Cloister_of_Storms/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_ramuh_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_ramuh_done",3);
		player:delKeyItem(DOMINAS_VIOLET_SEAL);
		player:addKeyItem(VIOLET_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,VIOLET_COUNTERSEAL);
	elseif(EventFinishBCNM(player,csid,option))then
		return;
	end
end;