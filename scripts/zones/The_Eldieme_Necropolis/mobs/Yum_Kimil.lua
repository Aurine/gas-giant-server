-----------------------------------
-- Area: The Eldieme Necropolis
-- NPC: Yum Kimil
-- Quests: The Requiem (BARD AF2)
-- @zone 195
-- @pos -414.000 8.000 499.000
-----------------------------------
package.loaded["scripts/zones/The_Eldieme_Necropolis/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/The_Eldieme_Necropolis/TextIDs");
require("scripts/globals/settings");

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- OnMobDeath Action
-----------------------------------
function onMobDeath(mob, killer)
	killer:setVar("TheRequiemYumKilled",1);
end;