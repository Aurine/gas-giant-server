-----------------------------------
-- Area: Windurst Waters
-- Door: Acolyte Hostel 11
-- Type: Object NPC
-- @zone 238
-- @pos 124.000, -3.000, 222.215
-----------------------------------
package.loaded["scripts/zones/Windurst_Waters/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/globals/settings");
require("scripts/zones/Windurst_Waters/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getQuestStatus(WINDURST,NOTHING_MATTERS) == QUEST_ACCEPTED and player:getVar("NothingMatters_Qs") == 6
	and player:getVar("NothingMatters_Door_11") == 0) then
		player:startEvent(0x032E);
	else
		player:showText(player,NOTHING_OUT_OF_ORDINARY);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x032E) then
		player:addKeyItem(THESIS_ON_ALCHEMY);
		player:messageSpecial(KEYITEM_OBTAINED,THESIS_ON_ALCHEMY);
		player:setVar("NothingMatters_Door_11",1);
		player:setVar("NothingMatters_Progress",3);
		player:setVar("NothingMatters_Qs",0);
		player:setVar("NothingMatters_Door_1",0);
		player:setVar("NothingMatters_Door_2",0);
		player:setVar("NothingMatters_Door_3",0);
		player:setVar("NothingMatters_Door_8",0);
		player:setVar("NothingMatters_Door_9",0);
		player:setVar("NothingMatters_Door_10",0);
	end
end;