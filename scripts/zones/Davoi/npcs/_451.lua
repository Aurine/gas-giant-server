-----------------------------------
-- Area: Davoi
-- NPC: _451 (Elevator Lever)
-- @zone 149
-- @pos 27.588 -2.655 -149.319
-- Notes: Used to operate Elevator
-- @450
-----------------------------------
package.loaded["scripts/zones/Davoi/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Davoi/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	npc:openDoor(3); -- Lever animation
	RunElevator(23);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option,npc)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;