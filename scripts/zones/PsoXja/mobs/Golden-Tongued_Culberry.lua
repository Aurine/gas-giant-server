-----------------------------------
-- Area: Pso'xja
--  NM:  Golden-Tongued Culberry 
-- @zone 9
-- @pos -271.214 31.838 260.258
-----------------------------------


-----------------------------------
-- onMobFight Action
-----------------------------------

function onMobFight(mob,target)
    mob:SetAutoAttackEnabled(false);
	mob:SetMobAbilityEnabled(false);
	mob:setMobMod(MOBMOD_MAGIC_COOL, 6);
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	GetNPCByID(16814434):hideNPC(900); -- qm1
end;