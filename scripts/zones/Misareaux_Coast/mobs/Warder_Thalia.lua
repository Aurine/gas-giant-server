-----------------------------------
-- Area: Misareaux Coast
-- NM: Warder Thalia
-- Missions: CoP 6-2
-- @zone 25
-- @pos 276.303 23.592 -396.958
-----------------------------------

require("scripts/globals/missions");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == A_PLACE_TO_RETURN and killer:getVar("PromathiaStatus") == 1)then 
		killer:setVar("Warder_Thalia_KILL",1);
	end
end;