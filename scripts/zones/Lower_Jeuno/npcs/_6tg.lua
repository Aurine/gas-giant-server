-----------------------------------
-- Area: Lower Jeuno
-- Door: Tenshodo HQ
-- @zone 245
-- @pos 38.250, 1.948, -57.765
-- cutscenes 0x0014  0x274e  0x274f  0x2750
-- 0x2751  0x2752  0x2753  0x2754  0x2755  0x2756  0x2757  0x2758  0x278d
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	npc:openDoor();
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;