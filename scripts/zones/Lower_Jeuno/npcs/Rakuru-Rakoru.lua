-----------------------------------
-- Area: Lower Jeuno
-- NPC: Rakuru-Rakoru
-- Type: Quest Giver
-- Quests: The Miraculous Dale
-- @zone 245
-- @pos -17.081, -0.449, 1.077
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/quests");
require("scripts/globals/keyitems");
require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local DALEQuest = player:getVar("DALEQuest");
	local var1 = 0;
	local var2 = 0;
	local var3 = 0;
	local var4 = 0;
	local var5 = 0;
	local var6 = 0;
	local var7 = 0;
	if(player:getMaskBit(DALEQuest,13) == true and player:getMaskBit(DALEQuest,5) == true and player:getMaskBit(DALEQuest,3) == true) then -- var 1
		var1 = 1;
	end
	if(player:getMaskBit(DALEQuest,14) == true and player:getMaskBit(DALEQuest,6) == true and player:getMaskBit(DALEQuest,4) == true) then -- var 2
		var2 = 1;
	end
	if(player:getMaskBit(DALEQuest,1) == true and player:getMaskBit(DALEQuest,15) == true) then -- var 3
		var3 = 1;
	end
	if(player:getMaskBit(DALEQuest,0) == true and player:getMaskBit(DALEQuest,9) == true) then -- var 4
		var4 = 1;
	end
	if(player:getMaskBit(DALEQuest,7) == true and player:getMaskBit(DALEQuest,8) == true) then -- var 5
		var5 = 1;
	end
	if(player:getMaskBit(DALEQuest,10) == true and player:getMaskBit(DALEQuest,11) == true) then -- var 6
		var6 = 1;
	end
	if(player:getMaskBit(DALEQuest,12) == true and player:getMaskBit(DALEQuest,2) == true) then -- var 7
		var7 = 1;
	end
	if(player:getMainLvl() >= 75 and player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_AVAILABLE) then
		player:startEvent(0x275F);
	elseif(player:isMaskFull(DALEQuest,15) == true) then
		player:startEvent(0x2762);
	elseif(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		player:startEvent(0x2764,0,var1,var2,var3,var4,var5,var6,var7);
	elseif(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_COMPLETED) then
		player:startEvent(0x2763);
	else
		player:startEvent(0x275E);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if(csid == 0x275F and option == 54) then
		if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_AVAILABLE) then
			player:addQuest(JEUNO,THE_MIRACULOUS_DALE);
			player:addKeyItem(DATA_ANALYZER_AND_LOGGER_EX);
			player:messageSpecial(KEYITEM_OBTAINED, DATA_ANALYZER_AND_LOGGER_EX);
		end
	elseif(csid == 0x2762) then
		player:completeQuest(JEUNO,THE_MIRACULOUS_DALE);
		player:delKeyItem(DATA_ANALYZER_AND_LOGGER_EX);
		player:addFame(JEUNOO,30);
		player:addGil(GIL_RATE*59630);
		player:messageSpecial(GIL_OBTAINED,GIL_RATE*59630);
	end
end;


-- player:startEvent(0x2764,0,0,0,0,0,0,0,0); -- none marked
-- player:startEvent(0x2764,1,0,0,0,0,0,0,0); -- none marked
-- player:startEvent(0x2764,2,0,0,0,0,0,0,0); -- none marked
-- player:startEvent(0x2764,3,0,0,0,0,0,0,0); -- none marked
-- player:startEvent(0x2764,0,1,0,0,0,0,0,0); -- Tumbling Truffle, Buburimboo, Boreal Hound
-- player:startEvent(0x2764,0,0,1,0,0,0,0,0); -- Tottering Toby, Daggerclaw Dracos, Boreal Tiger
-- player:startEvent(0x2764,0,0,0,1,0,0,0,0); -- Blubbery Bulge, Trickster Kinetix
-- player:startEvent(0x2764,0,0,0,0,1,0,0,0); -- Black Triple Stars, Ixtab
-- player:startEvent(0x2764,0,0,0,0,0,1,0,0); -- Drooling Daisy, Gargantua
-- player:startEvent(0x2764,0,0,0,0,0,0,1,0); -- Jolly Green, Shadow Eye
-- player:startEvent(0x2764,0,0,0,0,0,0,0,1); -- Sharp-Eared Ropipi, Boreal Coeurl