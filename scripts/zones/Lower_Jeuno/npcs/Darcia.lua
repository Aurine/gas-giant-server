-----------------------------------
-- Area: Lower Jeuno
-- NPC: Darcia
-- Type: Quest Giver
-- Mission: (SoA) 1-2
-- @zone 245
-- @pos -28.768, -2, -11.300
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/missions");
require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x2785);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
	if(csid == 0x2785 and option == 1) then

	elseif(csid == 0x2785 and option == 2) then
		if(player:getGil() < 1000000) then
			player:showText(npc,10159);
		else
			player:delGil(1000000);
			player:addKeyItem(ANDOULINIAN_CHARTER_PERMIT);
			player:messageSpecial(KEYITEM_OBTAINED,ANDOULINIAN_CHARTER_PERMIT);
			player:addKeyItem(GEOMAGNETRON);
			player:messageSpecial(KEYITEM_OBTAINED,GEOMAGNETRON);
		end
	elseif(csid == 0x2785 and option == 3) then
		if(player:hasKeyItem(TEMPORARY_GEOMAGNETRON) == false) then
			player:addKeyItem(TEMPORARY_GEOMAGNETRON);
			player:messageSpecial(KEYITEM_OBTAINED,TEMPORARY_GEOMAGNETRON);
		end
	end
end;