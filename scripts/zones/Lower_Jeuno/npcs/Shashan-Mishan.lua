-----------------------------------
-- Area: Lower Jeuno
-- NPC: Shashan-Mishan
-- Type: Weather NPC
-- @zone 245
-- @pos -113.449, 0.000, -167.358
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local birth = 1009810800;
	local timer = os.time();
	local counter = (timer - birth);
	player:startEvent(0x8271c,0,0,0,0,0,0,0,counter);
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;