-----------------------------------
-- Area: Port Windurst
-- NPC: Three of Clubs
-- Type: Quest NPC
-- Quests: Lure of the Wildcat
-- (Windurst)
-- @zone 240
-- @pos -7.238 -5.000 106.982
-----------------------------------
package.loaded["scripts/zones/Port_Windurst/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/zones/Port_Windurst/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local WildcatWindurst = player:getVar("WildcatWindurst");

	if (player:getQuestStatus(WINDURST,LURE_OF_THE_WILDCAT_WINDURST) == QUEST_ACCEPTED and player:getMaskBit(WildcatWindurst,18) == false) then
		player:startEvent(0x0271);
	else
		player:startEvent(0x00de);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if (csid == 0x0271) then
		player:setMaskBit(player:getVar("WildcatWindurst"),"WildcatWindurst",18,true);
	end	

end;
