-----------------------------------
-- Area: Port Windurst
-- NPC: Kameel
-- Type: Adventurer's Assistant
-- @zone 240
-- @pos 220.994 -6.834 89.828
-----------------------------------
package.loaded["scripts/zones/Port_Windurst/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	if (TRAVEL_SKIP >= 1) then
		if (trade:getGil() >= TRAVEL_SKIP and trade:getItemCount() == 1) then
			player:delGil(TRAVEL_SKIP);
			player:setPos(16,12,-120,192,246);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x00c1);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;