-----------------------------------
-- Area: Northern San d'Oria
-- NPC: Cheupirudaux
-- Type: Woodworking Guildmaster
-- Quests: It's Raining Mannequins! 
-- @zone 231
-- @pos -138.163 11.999 250.949
-----------------------------------
package.loaded["scripts/zones/Northern_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/globals/crafting");
require("scripts/zones/Northern_San_dOria/TextIDs");

local SKILLID = 49; -- Woodworking

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	local newRank = tradeTestItem(player,npc,trade,SKILLID);

	if(newRank ~= 0) then
		player:setSkillRank(SKILLID,newRank);
		player:startEvent(0x026e,0,0,0,0,newRank);
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local getNewRank = 0;
	local craftSkill = player:getSkillLevel(SKILLID);
	local testItem = getTestItem(player,npc,SKILLID);
	local guildMember = isGuildMember(player,9);
	if(guildMember == 1) then
		guildMember = 150995375;
	end
	if(canGetNewRank(player,craftSkill,SKILLID) == 1) then
		getNewRank = 100;
	end

	if(player:getQuestStatus(OTHER_AREAS,IT_S_RAINING_MANNEQUINS) == QUEST_ACCEPTED and
		player:getVar("Raining_M_Progress") == 3) then
		player:startEvent(0x02F7);
	else
		player:startEvent(0x026d,testItem,getNewRank,30,guildMember,44,0,0,0);
	end

end;

-- 0x026d  0x026e  0x02f7  0x0010  0x0000

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);

	if(csid == 0x026d and option == 1) then
		signupGuild(player,512);

		if(player:getFreeSlotsCount() == 0) then 
			player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,4098);
		else
			player:addItem(4098);
			player:messageSpecial(ITEM_OBTAINED,4098); -- Wind Crystal
		end
	elseif(csid == 0x02F7) then
		player:setVar("Raining_M_Progress",4);
		player:addKeyItem(MANNEQUIN_JOINT_DIAGRAMS);
		player:messageSpecial(KEYITEM_OBTAINED,MANNEQUIN_JOINT_DIAGRAMS);		
	end

end;