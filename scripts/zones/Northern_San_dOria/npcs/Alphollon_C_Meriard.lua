-----------------------------------
-- Area: Northern San d'Oria
-- NPC: Alphollon C Meriard
-- Type: Purifies cursed items with
-- their corresponding abjurations.
-- @zone 231
-- @pos 98.108 -1.000 137.999
-----------------------------------
package.loaded["scripts/zones/Northern_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Northern_San_dOria/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	-- Abjuration, Item, Item -1, Reward, Reward +1
	local abjuList = {
	1314,1344,1345,13934,13935,	1315,1346,1347,14387,14388, 	1316,1348,1349,14821,14822, 	1317,1350,1351,14303,14304,	1318,1352,1353,14184,14185,	-- Dryadic Abjuration (Shura/+1)
	1319,1354,1355,12429,13924,	1320,1356,1357,12557,14371, 	1321,1358,1359,12685,14816, 	1322,1360,1361,12813,14296,	1323,1362,1363,12941,14175,	-- Earthen Abjuration (Adaman/Armada)
	1324,1364,1365,13876,13877,	1325,1366,1367,13787,13788, 	1326,1368,1369,14006,14007, 	1327,1370,1371,14247,14248,	1328,1372,1373,14123,14124,	-- Aquarian Abjuration (Zenith/+1)
	1329,1374,1375,12421,13911,	1330,1376,1377,12549,14370, 	1331,1378,1379,12677,14061, 	1332,1380,1381,12805,14283,	1333,1382,1383,12933,14163,	-- Martial Abjuration (Koenig/Kaiser)
	1334,1384,1385,13908,13909,	1335,1386,1387,14367,14368, 	1336,1388,1389,14058,14059, 	1337,1390,1391,14280,14281,	1338,1392,1393,14160,14161,	-- Wyrmal Abjuration (Crimson/Blood)
	1339,1394,1395,13927,13928,	1340,1396,1397,14378,14379, 	1341,1398,1399,14076,14077, 	1342,1400,1401,14308,14309,	1343,1402,1403,14180,14181,	-- Neptunal Abjuration (Hecatomb/+1)
	2429,2439,2440,16113,16114,	2430,2441,2442,14573,14574, 	2431,2443,2444,14995,14996, 	2432,2445,2446,15655,15656,	2433,2447,2448,15740,15741,	-- Phantasmal Abjuration (Shadow/Valkyrie's)
	2434,2449,2450,16115,16116,	2435,2451,2452,14575,14576, 	2436,2453,2454,14997,14998, 	2437,2455,2456,15657,15658,	2438,2457,2458,15742,15743,	-- Hadean Abjuration (Shadow/Valkyrie's)
	3559,10419,10424,10400,10405,	3560,10240,10245,11873,10489, 	3561,10303,10308,10534,10539, 	3562,10583,10588,10565,10570,	3563,10353,10358,10631,10636,	-- Corvine Abjuration (Hrafn/Huginn)
	3574,10422,10427,10403,10408,	3575,10243,10248,11876,10492, 	3576,10306,10311,10537,10542, 	3577,10586,10591,10568,10573,	3578,10356,10361,10634,10639,	-- Foreboding Abjuration (Auspex/Spurrina)
	3579,10423,10428,10404,10409,	3580,10244,10249,11877,10493, 	3581,10307,10312,10538,10543, 	3582,10587,10592,10569,10574,	3583,10357,10362,10635,10640,	-- Lenitive Abjuration (Paean/Iaso)
	3564,10420,10425,10401,10406,	3565,10241,10246,11874,10490, 	3566,10304,10309,10535,10540, 	3567,10584,10589,10566,10571,	3568,10354,10359,10632,10637,	-- Supernal Abjuration (Tenryu/+1)
	3569,10421,10426,10402,10407,	3570,10242,10247,11875,10491, 	3571,10305,10310,10536,10541, 	3572,10585,10590,10567,10572,	3573,10355,10360,10633,10638,	-- Transitory Abjuration (Kheper/Khepri)

	1441,4234,4234,4513,4513,	1442,4235,4235,4511,4511}; -- Amrita & Ambrosia

	local item = 0;
	local reward = 0;

	if(trade:getItemCount() == 2) then

		for i = 1,table.getn(abjuList),5 do
			if(trade:hasItemQty(abjuList[i],1)) then
				if(trade:hasItemQty(abjuList[i + 1],1)) then
					item = abjuList[i + 1];
					reward = abjuList[i + 3];
				elseif(trade:hasItemQty(abjuList[i + 2],1)) then
					item = abjuList[i + 2];
					reward = abjuList[i + 4];
				end
				break;
			end
		end

		if (reward ~= 0) then
			-- Trade pair for a nice reward.
			player:startEvent(0x02d0,item,reward);
			player:setVar("reward",reward);
		end
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x02cf);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
-- printf("CSID: %u",csid);
-- printf("RESULT: %u",option);

	if(csid == 0x02d0 and player:getVar("reward") ~= 0) then
		local reward = player:getVar("reward");

		player:tradeComplete();
		player:addItem(reward);
		player:messageSpecial(ITEM_OBTAINED,reward);
		player:setVar("reward",0);
	end

end;