-----------------------------------
-- Area: Kuftal Tunnel
-- NM: Gizerl's Ghost
-- Missions: Bastok 8-2
-- @zone 174
-- @pos -25.000 -10.000 -152.000
-- @zone 174
-- @pos -25.000 -10.000 -152.000
-----------------------------------

require("scripts/globals/missions");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	if(killer:getCurrentMission(BASTOK) == ENTER_THE_TALEKEEPER and killer:getVar("MissionStatus") == 2) then
		killer:setVar("MissionStatus",3);
	end

end;
