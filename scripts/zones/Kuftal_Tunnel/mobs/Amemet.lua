----------------------------------	
-- Area: Kuftal Tunnel	
-- NM: Amemet
-- @zone 174
-- @pos 102.000 -0.191 3.000
-- TODO: Amemet should walk in a
-- big circle
-----------------------------------	

require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	-- Set Amemet's Window Open Time
    local wait = math.random((7200),(43200)); -- 2-12 hours
	SetServerVariable("[POP]Amemet", os.time(t) + (wait /NM_TIMER_MOD));
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Amemet");
	SetServerVariable("[PH]Amemet", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;