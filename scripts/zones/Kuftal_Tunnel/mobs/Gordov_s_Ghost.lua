-----------------------------------	
-- Area: Kuftal Tunnel	
-- NM: Gordov's Ghost
-- Missions: Bastok 8-2
-- @zone 174
-- @pos -26.000, -11.000, -143.000
-----------------------------------	

require("scripts/globals/missions");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	if(killer:getCurrentMission(BASTOK) == ENTER_THE_TALEKEEPER and killer:getVar("MissionStatus") == 2) then
		killer:setVar("MissionStatus",3);
	end

end;	
