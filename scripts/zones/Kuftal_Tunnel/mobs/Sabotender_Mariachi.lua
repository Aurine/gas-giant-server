----------------------------------	
-- Area: Kuftal Tunnel	
-- NM: Sabotender Mariachi
-- @zone 174
-- @pos -56.081 -1.546 21.705
-- TODO: Auto-Regen during the day
-----------------------------------	

require("scripts/globals/settings");

  
-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	-- Set Sabotender Mariachi's Window Open Time
    local wait = math.random((10800),(28800)); -- 3-8 hours
	SetServerVariable("[POP]Sabotender_Mariachi", os.time(t) + (wait /NM_TIMER_MOD)); -- 3-8 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Sabotender_Mariachi");
	SetServerVariable("[PH]Sabotender_Mariachi", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;