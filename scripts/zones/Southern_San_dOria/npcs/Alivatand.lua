-----------------------------------
-- Area: Southern San d`Oria
-- NPC: Alivatand
-- Type: Guildworkers Union
-- Leathercraft
-- Quests: Flyers for Regine
-- @zone 230
-- @pos -179.458 -1.000 15.857
-----------------------------------
-- Guild shops not implemented

package.loaded["scripts/zones/Southern_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/shop");
require("scripts/globals/quests");
require("scripts/zones/Southern_San_dOria/TextIDs");
-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	-- "Flyers for Regine" conditional script
	local FlyerForRegine = player:getQuestStatus(SANDORIA,FLYERS_FOR_REGINE);

	if (FlyerForRegine == 1) then
		local count = trade:getItemCount();
		local MagicFlyer = trade:hasItemQty(532,1);
		if (MagicFlyer == true and count == 1) then
			player:messageSpecial(FLYER_REFUSED);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:showText(npc,6692) -- Is a temp textid 

	local stock = {
			 4242,200,1,		-- Plasma Crystal
			 4238,200,1,		-- Inferno Crystal
			 4239,200,1,		-- Glacier Crystal
			 4240,200,1,		-- Cyclone Crystal
			 4241,200,1,		-- Terra Crystal
			 4243,200,1,		-- Torrent Crystal
			 4245,200,1,		-- Twilight Crystal
			 4244,200,1,		-- Aurora Crystal

			 15448,10000,2,		-- Tanners Belt
			 -- 2129,10000,2,		-- Tanning
			 -- 2016,40000,2,		-- Leather purification is a key item
			 -- 2017,40000,2,		-- Leather ensorcelment is a key item
			 14832,70000,3,		-- Tanners Gloves
			 15823,80000,3,		-- Tanners Ring
			 14396,100000,3,	-- Tanners Apron
			 3668,50000,3,		-- Hide Stretcher
			 202,150000,3,		-- Golden Fleece
			 3329,15000,3,		-- Tanners Emblem
			 339,200000,3}		-- Tanners Signboard

	showNationShop(player, SANDORIA, stock);

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
