----------------------------------	
-- Area: Cape Teriggan	
-- NM: Frostmane
-- @zone 113
-- @pos -281.000, -0.500, 471.000
-----------------------------------	

require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	-- Set Frostmane's Window Open Time
    local wait = math.random((3600),(21600)) -- 1-6 hours
	SetServerVariable("[POP]Frostmane", os.time(t) + (wait /NM_TIMER_MOD)); 
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Frostmane");
	SetServerVariable("[PH]Frostmane", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;