-----------------------------------
-- Area: Cape Teriggan
-- NM: Axesarion the Wanderer
-- @zone 113
-- @pos -107.000, -8.000, 454.00
-----------------------------------

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	
	if(GetServerVariable("[ZM4]Wind_Headstone_Active") == 0) then	
		SetServerVariable("[ZM4]Wind_Headstone_Active",os.time()+ 900);
	end
	
end;