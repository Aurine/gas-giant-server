-----------------------------------	
-- Area: Pashhow Marshlands	
-- MOB:  Jolly Green
-- Quests: The Miraculous Dale
-- @zone 109
-- @pos 276.084, 24.146, -1.695
-----------------------------------	
	
require("/scripts/globals/fieldsofvalor");	
require("scripts/globals/quests");
	
-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	
	checkRegime(killer,mob,60,3);
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",10,true);
	end
end;	
