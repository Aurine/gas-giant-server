-----------------------------------
-- Area: Konschtat Highlands
-- NM: Stray Mary
-- @zone 108
-- @pos -212.268 39.477 329.581
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(MARYS_GUIDE);
end;