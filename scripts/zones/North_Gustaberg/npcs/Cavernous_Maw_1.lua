-----------------------------------
-- Area: North Gustaberg
-- Name: Cavernous Maw
-- Teleports Players to
-- Abyssea-Grauberg
-- @zone 106
-- @pos -78 -0.5 600
-----------------------------------
package.loaded["scripts/zones/North_Gustaberg/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/teleports");
require("scripts/globals/campaign");
require("scripts/zones/North_Gustaberg/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (player:getMainLvl() >= ABYSSEA_MIN_LV) then
		player:startEvent(0x038C,1,1,1,1,1,1,1);
	else
		player:messageSpecial(NOTHING_HAPPENS);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
   
-----------------------------------
-- onEventFinish Action
-----------------------------------
function onEventFinish(player,csid,option)
	-- print("CSID:",csid);
	-- print("RESULT:",option);
	if(csid == 0x038C and option == 1) then
		player:setPos(-555,31,-760,0,254); 
	end
end;