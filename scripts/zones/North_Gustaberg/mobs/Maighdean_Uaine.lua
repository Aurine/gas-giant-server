----------------------------------
-- Area: North Gustaberg
-- NM: Maighdean Uaine
-- @zone 106
-- @pos 272.000 -0.324 797.800
-----------------------------------

require("scripts/globals/settings");
require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Maighdean_Uaine's Window Open Time
	local wait = math.random((900),(10800))
	SetServerVariable("[POP]Maighdean_Uaine", os.time(t) + (wait /NM_TIMER_MOD)); -- 15-180 minutes
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	local PH = GetServerVariable("[PH]Maighdean_Uaine");
	SetServerVariable("[PH]Maighdean_Uaine", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;