-----------------------------------
-- Area: VeLugannon Palace
-- NPC:  Zipacna
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	mob:setRespawnTime((math.random((10800),(14400))) /NM_TIMER_MOD); -- respawn 3-4 hrs
end;