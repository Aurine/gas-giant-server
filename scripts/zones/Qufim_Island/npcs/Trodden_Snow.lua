-----------------------------------
-- Area: Qufim Island
-- NPC: Trodden Snow
-- Type: NPC Mission Requirement
-- @zone 134
-- @pos -19.775 -17.300 104.400
-----------------------------------
package.loaded["scripts/zones/Qufim_Island/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/shop");
require("scripts/globals/conquest");
require("scripts/globals/missions");
require("scripts/globals/keyitems");
require("scripts/zones/Qufim_Island/TextIDs");


-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local CurrentMission = player:getCurrentMission(ASA);
	local Count = trade:getItemCount();

	if (CurrentMission == THAT_WHICH_CURDLES_BLOOD and player:getVar("MissionStatus") == 2 and trade:hasItemQty(2779,1) and Count == 1) then
		player:startEvent(0x002C);
		player:tradeComplete();
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local CurrentMission = player:getCurrentMission(ASA);

	if (CurrentMission == SUGAR_COATED_DIRECTIVE and player:getVar("ASA_primals_count") > 3) then
		player:startEvent(0x002D);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("OPTION: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("OPTION: %u",option);

	if (csid == 0x002C) then
		player:setVar("ASA_primals_count",0); -- We have to clear these, in case they helped do this mission before, they now have to do it themselves again.
		player:setVar("ASA_garuda_done",0);
		player:setVar("ASA_ramuh_done",0); -- Above comment likely wrong, they should not be set of players who don't need them,
		player:setVar("ASA_shiva_done",0); -- and should be cleared on use by those that do so they shouldn't need to be cleared HERE at all.
		player:setVar("ASA_ifrit_done",0); -- Leaving this here to remind myself to look into it and refactor if needed.
		player:setVar("ASA_titan_done",0);
		player:setVar("ASA_leviathan_done",0);
		player:addKeyItem(DOMINAS_SCARLET_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_SCARLET_SEAL);
		player:addKeyItem(DOMINAS_CERULEAN_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_CERULEAN_SEAL);
		player:addKeyItem(DOMINAS_EMERALD_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_EMERALD_SEAL);
		player:addKeyItem(DOMINAS_AMBER_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_AMBER_SEAL);
		player:addKeyItem(DOMINAS_VIOLET_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_VIOLET_SEAL);
		player:addKeyItem(DOMINAS_AZURE_SEAL);
		player:messageSpecial(KEYITEM_OBTAINED,DOMINAS_AZURE_SEAL);
		player:completeMission(ASA,THAT_WHICH_CURDLES_BLOOD);
		player:addMission(ASA,SUGAR_COATED_DIRECTIVE);
	elseif (csid == 0x002D) then
		player:completeMission(ASA,SUGAR_COATED_DIRECTIVE);
		player:addMission(ASA,ENEMY_OF_THE_EMPIRE);

		if (player:hasKeyItem(EMERALD_COUNTERSEAL)) then
			player:delKeyItem(EMERALD_COUNTERSEAL);
		end

		if (player:hasKeyItem(VIOLET_COUNTERSEAL)) then
			player:delKeyItem(VIOLET_COUNTERSEAL);
		end

		if (player:hasKeyItem(AZURE_COUNTERSEAL)) then
			player:delKeyItem(AZURE_COUNTERSEAL);
		end

		if (player:hasKeyItem(SCARLET_COUNTERSEAL)) then
			player:delKeyItem(SCARLET_COUNTERSEAL);
		end

		if (player:hasKeyItem(AMBER_COUNTERSEAL)) then
			player:delKeyItem(AMBER_COUNTERSEAL);
		end

		if (player:hasKeyItem(CERULEAN_COUNTERSEAL)) then
			player:delKeyItem(CERULEAN_COUNTERSEAL);
		end

		if (player:getVar("ASA_primals_count") == 3) then
			gil = 3000;
		elseif (player:getVar("ASA_primals_count") == 4) then
			gil = 1000;
		elseif (player:getVar("ASA_primals_count") == 5) then
			gil = 30000;
		elseif (player:getVar("ASA_primals_count") == 6) then
			gil = 50000;
		end
		player:addGil(gil*GIL_RATE);
		player:messageSpecial(GIL_OBTAINED,gil*GIL_RATE);
	end

end;