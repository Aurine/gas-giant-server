-----------------------------------
-- Area: The Shrine of Ru'Avitau
-- NPC:  Faust
-- @zone 178
-- @pos 740.000 -0.463 -99.000
-----------------------------------

-- TODO: Faust should WS ~3 times in a row each time. 
require("scripts/globals/settings");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	mob:setRespawnTime((math.random((10800),(21600))) /NM_TIMER_MOD); -- respawn 3-6 hrs
end;