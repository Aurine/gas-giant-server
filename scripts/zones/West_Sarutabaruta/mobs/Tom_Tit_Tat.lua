-----------------------------------
-- Area: West Sarutabaruta
-- NM:   Tom Tit Tat
-- @zone 115
-- @pos 139.943, -35.719, 299.835
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    -- Set Tom_Tit_Tat's Window Open Time
    local wait = math.random((3600),(7200))
    SetServerVariable("[POP]Tom_Tit_Tat", os.time(t) + (wait /NM_TIMER_MOD)); -- 1- 2 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Tom_Tit_Tat");
    SetServerVariable("[PH]Tom_Tit_Tat", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;
