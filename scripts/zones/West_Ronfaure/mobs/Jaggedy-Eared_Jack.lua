-----------------------------------
-- Area: West Ronfaure (100)
--  NM:  Jaggedy-Eared_Jack
-- @zone 100
-- @pos -281.000, -19.000, -220.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    -- Set Jaggedy-Eared_Jack's Window Open Time, and disable respawn
    local wait = math.random((300),(21600));
    -- On retail Jack is pure lotto, PH repops every 5 min and has very low chance to be Jack instead.
    SetServerVariable("[POP]Jaggedy_Eared_Jack", os.time(t) + (wait /NM_TIMER_MOD)); -- 5 minutes to 6 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Jaggedy_Eared_Jack");
    SetServerVariable("[PH]Jaggedy_Eared_Jack", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;

