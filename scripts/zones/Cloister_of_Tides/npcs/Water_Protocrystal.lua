-----------------------------------
-- Area: Cloister of Tides
-- NPC: Water Protocrystal
-- Quests: Trial by Water, Trial
-- Size Trial by Water
-- @zone 211
-- @pos 558.986 35.099 562.958
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Tides/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-------------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/globals/missions");
require("scripts/zones/Cloister_of_Tides/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_leviathan_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_leviathan_done",3);
		player:delKeyItem(DOMINAS_CERULEAN_SEAL);
		player:addKeyItem(CERULEAN_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,CERULEAN_COUNTERSEAL);
	elseif(EventFinishBCNM(player,csid,option))then
		return;
	end

end;