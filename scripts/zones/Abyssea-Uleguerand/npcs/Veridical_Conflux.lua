-----------------------------------
-- Area: Abyssea
-- NPC: Veridical Conflux
-- Type: Aybssea Teleport NPC
-- @zone 253
-- @pos -210.000 -40.400 -498.000
-- @pos -382.247 -24.605 -173.394
-- @pos -301.040 -53.416 -32.147
-- @pos 134.289 -0.449 -367.906
-- @pos 572.884 -36.808 -9.127
-- @pos 340.583 -100.672 502.408
-- @pos -260.289 -175.970 239.178
-- @pos -580.360 -40.205 49.438
-----------------------------------
package.loaded["scripts/globals/conflux"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/conflux");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local csid, param1, param2, param3, param4, param5, param6, param7, param8 = startConflux(player,npc);
	player:startEvent(csid, param1, param2, param3, param4, param5, param6, param7, param8);
	-- printf("csid:%u, param1:%u, param2:%u, param3:%u, param4:%u, param5:%u, param6:%u, param7:%u, param8:%u", csid, param1, param2, param3, param4, param5, param6, param7, param8);

end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("Update: CSID: %u",csid);
	-- printf("Update: RESULT: %u",option);

	player:updateEvent(1,0,0,0,0,0,0,0);

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("Finish: CSID: %u",csid);
	-- printf("Finish: RESULT: %u",option);

	finishConflux(player,csid,option);

end;