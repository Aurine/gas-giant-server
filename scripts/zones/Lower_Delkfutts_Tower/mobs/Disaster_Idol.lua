-----------------------------------
-- Area: Lower Delkfutt's Tower
-- NM: Disaster Idol
-- Mission: CoP 5-3
-- @zone 184
-- @pos 459.000 0.100 132.000
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/missions");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == THREE_PATHS and killer:getVar("COP_Tenzen_s_Path") == 6)then 
		killer:setVar("COP_Tenzen_s_Path",7);
	end
end;