-----------------------------------
-- Area: Newton Movalpolos
-- NM: Bugbear Matman
-- @zone 12
-- @pos 124.544 19.988 -60.670
-----------------------------------

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end; 

-----------------------------------
-- OnMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	GetNPCByID(16826573):hideNPC(900); -- Moblin Showman in NPC_List
end;