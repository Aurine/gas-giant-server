-----------------------------------
-- Area: Mordion Gaol
-- NPC: Moogle
-- Type: GM Jailer
-- @zone 131
-- @pos 180.000 12.000 668.000
-- @pos 620.000 12.000 228.000
-- @pos 180.000 11.000 228.000
-- @pos -619.000 12.000 -211.000
-------------------------------------
package.loaded["scripts/zones/Mordion_Gaol/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/settings");
require("scripts/zones/Mordion_Gaol/TextIDs");

----------------------------------- 
-- onTrade Action 
----------------------------------- 

function onTrade(player,npc,trade) 
end;

----------------------------------- 
-- onTrigger Action 
-----------------------------------
 
function onTrigger(player,npc) 
	player:startEvent(0x0067);
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;