-----------------------------------
-- Area: Crawler's Nest
-- NM: Drone Crawler 
-- @pos 197
-- @pos 4.045 -2.703 285.026
-- (Spawn 1)
-- @pos -74.939 -2.606 244.139
-- (Spawn 2)
-- Note: Spawned by trading a
-- Rolanberry 881 to the basket at
-- H-7 or G-8 on the second map.
-- Does not spawn 100% of the time;
-- a message displaying "Nothing
-- happened" may appear, which
-- still uses up a Rolanberry 881. 
-----------------------------------


-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------


function onMobDeath(mob,killer)

	mob = mob:getID();

	if (mob == 17584131) then
		GetNPCByID(17584455):hideNPC(900); -- qm3

	elseif (mob == 17584132) then
		GetNPCByID(17584456):hideNPC(900);-- qm4

	end

end;
