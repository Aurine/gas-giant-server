-----------------------------------
-- Area: Crawler's Nest
-- NM: Awd Goggie
-- @zone 197
-- @pos -253.026 -1.867 253.055
-- Note: Spawned by trading a
-- Rolanberry 864 to the basket at
-- E-7 on the second map. Does not
-- spawn 100% of the time; can
-- display the message "Nothing
-- Happened", which still uses up
-- a Rolanberry 864. 
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(BOGEYDOWNER);
	GetNPCByID(17584459):hideNPC(900); -- qm7
end;