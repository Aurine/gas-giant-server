-----------------------------------
-- Area: Sacrarium
-- NPC: _0s5 (Wooden Gate)
-- @zone 28
-- @pos 100.000, -3.500, -97.370
-----------------------------------


-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(npc:getAnimation() == 9) then
		npc:openDoor();	
	end	
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;