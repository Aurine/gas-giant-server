-----------------------------------
-- Area: Sauromugue Champaign (120)
-- Mob: Goblin's Beetle
-- @zone 120
-- @pos many
-----------------------------------
package.loaded["scripts/zones/Sauromugue_Champaign/TextIDs"] = nil;
-----------------------------------

-- require("scripts/zones/Sauromugue_Champaign/MobIDs");
require("/scripts/globals/fieldsofvalor");
require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/zones/Sauromugue_Champaign/TextIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	if(ENABLE_ACP == 1 and (killer:hasKeyItem(CHUNK_OF_SMOKED_GOBLIN_GRUB) == false) and killer:getCurrentMission(ACP) >= THE_ECHO_AWAKENS) then
		-- Guesstimating 15% chance
		if (math.random((1),(100)) >= 85) then
			killer:addKeyItem(CHUNK_OF_SMOKED_GOBLIN_GRUB);
			killer:messageSpecial(KEYITEM_OBTAINED,CHUNK_OF_SMOKED_GOBLIN_GRUB);
		end
	end

end;