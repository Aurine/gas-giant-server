-----------------------------------
-- Area: Bastok Mines
-- NPC: Hunt Registry
-- @zone 234
-- @pos 18.000, 0.000, -120.000
-----------------------------------
package.loaded["scripts/zones/Bastok_Mines/TextIDs"] = nil;
-----------------------------------

require("/scripts/globals/hunt_registry");
require("scripts/zones/Bastok_Mines/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	startHunt(HuntEvent_Bastok_Mines,player);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
    -- if (csid == 1500 and option == 1) then
    -- elseif (csid == 1500 and option == 2) then
    -- elseif (csid == 1500 and option == 3) then
    -- else
    if (option == 4) then
        player:setVar("fov_regimeid",0);
        player:setVar("fov_numkilled1",0);
        player:setVar("fov_numkilled2",0);
        player:setVar("fov_numkilled3",0);
        player:setVar("fov_numkilled4",0);
        -- player:setVar("gov_regimeid",0);
        -- player:setVar("gov_numkilled1",0);
        -- player:setVar("gov_numkilled2",0);
        -- player:setVar("gov_numkilled3",0);
        -- player:setVar("gov_numkilled4",0);
    end
end;