-----------------------------------
-- Area: Bastok_Mines
-- NPC: Rodellieux
-- Type: Regional Merchant NPC
-- @zone 234
-- @pos -18.612 -0.044 -0.643
-- Notes: Only sells when Bastok
-- controls Fauregandi Region
-----------------------------------
package.loaded["scripts/zones/Bastok_Mines/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/shop");
require("scripts/globals/conquest");
require("scripts/zones/Bastok_Mines/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local  RegionOwner = GetRegionOwner(FAUREGANDI);

	if (RegionOwner ~= BASTOK) then 
		player:showText(npc,RODELLIEUX_CLOSED_DIALOG);
	else
		player:showText(npc,RODELLIEUX_OPEN_DIALOG);
		local stock = 
		{
			0x11db,    90,   -- Beaugreens
			0x110b,    39,   -- Faerie Apple
			0x02b3,    54    -- Maple Log
		}
		showShop(player,BASTOK,stock);

	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
