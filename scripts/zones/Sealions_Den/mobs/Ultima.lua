-----------------------------------
-- Area: Sealion's Den
--  NM:  Ultima
-- @zone 32
-- @pos -640.000 -231.000 516.000
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobInitialize Action
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(ULTIMA_UNDERTAKER);
end;