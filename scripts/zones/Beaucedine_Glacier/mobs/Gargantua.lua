-----------------------------------
-- Area: Beaucedine Glacier (111)
-- NM:  Gargantua
-- Quest: The Mirasculous Dale
-- @zone 111
-- @pos 348.000, -0.500, 17.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/quests");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Gargantua's Window Open Time
	wait = math.random((3600),(25200));
	SetServerVariable("[POP]Gargantua", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-7 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	PH = GetServerVariable("[PH]Gargantua");
	SetServerVariable("[PH]Gargantua", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",8,true);
	end
end;
