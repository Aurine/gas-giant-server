-----------------------------------
-- Area: Beaucedine Glacier
-- NM: Lord Ruthven
-- @zone 111
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(RUTHVEN_ENTOMBER);
end;