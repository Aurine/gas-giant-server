-----------------------------------
-- Area: Outer Ra'Kaznar
-- NPC: Augural Conveyor
-- Zones to Outer Ra'Kaznar [U] (zone 275)
-- @zone 274
-- @pos -34.549 -181.334 -20.031
-----------------------------------
package.loaded["scripts/zones/Outer_RaKaznar/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Outer_RaKaznar/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    player:startEvent(0x001f);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
    if (csid == 0x001f and option == 1) then
        player:setPos(-40,-180,-20,128,275);
    end
end;