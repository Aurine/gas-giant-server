-----------------------------------
--  Area: Western Altepa Desert (125)
--    NM: Cactuar_Cantautor
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Cactuar_Cantautor's Window Open Time
    wait = math.random((3600),(43200));
    SetServerVariable("[POP]Cactuar_Cantautor", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-12 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Cactuar_Cantautor");
    SetServerVariable("[PH]Cactuar_Cantautor", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;

