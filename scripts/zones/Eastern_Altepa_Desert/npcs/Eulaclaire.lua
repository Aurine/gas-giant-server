-----------------------------------
-- Area: Eastern Altepa Desert
-- NPC: Eulaclaire
-- Type: Chocobo Renter
-- @zone 114
-- @pos -55.715, 3.949, 232.524
-----------------------------------
package.loaded["scripts/zones/Eastern_Altepa_Desert/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/Eastern_Altepa_Desert/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();
	local count = trade:getItemCount();
	local ChocoboTicket = trade:hasItemQty(1514,1);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;

	if(ChocoboTicket == true and count == 1) then
		if (hasLicense and level >= 20) then
			player:tradeComplete();
			player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
		end
	end
end;


-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local price = 100;
	local gil = player:getGil();
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();
	local ChocobosWounds = player:getQuestStatus(JEUNO,CHOCOBO_S_WOUNDS);

	if ((hasLicense and level >= 15) or (level >=15 and ChocobosWounds == QUEST_COMPLETED)) then
		player:startEvent(0x0006,price,gil);
	else
		player:startEvent(0x0007,price,gil);
	end

end;

  
-----------------------------------
-- onEventFinish Action
-----------------------------------
function onEventFinish(player,csid,option)
--print("CSID:",csid);
--print("OPTION:",option);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;
	local price = 100;

	if (csid == 0x0006 and option == 0) then
		if (player:delGil(price)) then
			if (player:getMainLvl() >= 20) then
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			else
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(900 + durationMod),true);
			end
		end
	end
end;