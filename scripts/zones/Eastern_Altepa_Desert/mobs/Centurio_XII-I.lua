-----------------------------------
--  Area: Eastern Altepa Desert (114)
--    NM: Centurio_XII-I
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Centurio XII-I's spawnpoint and respawn time (21-24 hours)
	UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;

