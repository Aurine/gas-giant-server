-----------------------------------
-- Area: Cloister of Flames
-- NM: Ifrit Prime
-- Quests: Trial by Fire
-- @zone 207
-- @pos 
-----------------------------------

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
	if (mob:getMobByID() >= 1762510 and mob:getMobByID() <= 1762512) then
		mob:setExtraVar(1,0);
		mob:setExtraVar(3,0);
		mob:setUnkillable(true);
	end
end;

-----------------------------------
-- OnMobDeath Action
-----------------------------------

function onMobDeath(mob, killer)
end;

function onMobFight(mob, target)

	if (target:getCurrentMission(ASA) == SUGAR_COATED_DIRECTIVE) then
		health = mob:getHPP();
		mob:setExtraVar(2,0);

		if (health <= 80 and mob:getExtaVar(4) == 0) then
			mob:useMobAbility(542);
			mob:setExtraVar(4,1);
			mob:setExtraVar(3,mob:getExtraVar(3)+1);
		elseif (health <= 60 and mob:getExtaVar(5) == 0) then
			mob:useMobAbility(542);
			mob:setExtraVar(5,1);
			mob:setExtraVar(3,mob:getExtraVar(3)+1);
		elseif (health <= 40 and mob:getExtaVar(6) == 0) then
			mob:useMobAbility(542);
			mob:setExtraVar(6,1);
			mob:setExtraVar(3,mob:getExtraVar(3)+1);
		elseif (health <= 20 and mob:getExtaVar(6) == 0) then
			mob:useMobAbility(542);
			mob:setExtraVar(6,1);
			mob:setExtraVar(3,mob:getExtraVar(3)+1);
		elseif (health <= 2) then
			if (mob:getExtraVar(3) >= 5) then
				mob:setUnkillable(false);
			else
				mob:useMobAbility(542);
			end
		end

	end

end;