-----------------------------------
-- Area: Dynamis - Beaucedine
-- NPC:  qm5 (???)
-- @zone 
-- @pos 
-- Used to Spawn: Velosareon
-----------------------------------
package.loaded["scripts/zones/Dynamis-Beaucedine/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Dynamis-Beaucedine/TextIDs");

-----------------------------------
-- onTrigger
-----------------------------------

function onTrigger(player,npc)
end;

-----------------------------------
-- onTrade
-----------------------------------

function onTrade(player,npc,trade)
	local Velosareon = GetMobAction(17326089);

	if (trade:hasItemQty(3362) and trade:getItemCount() == 1 and (Velosareon == ACTION_NONE or Velosareon == ACTION_SPAWN) ) then
		SpawnMob(17326089,180):updateEnmity(player);
		player:tradeComplete();
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;