----------------------------------
-- Area: Gustav Tunnel
-- NM: Wyvernpoacher Drachlox
-- @zone 212
-- @pos -63.000 1.000 -61.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Wyvernpoacher Drachlox's Window Open Time
    local wait = math.random((7200),(28800));  -- 2-8 hours
	SetServerVariable("[POP]Wyvernpoacher_Drachlox", os.time(t) + (wait /NM_TIMER_MOD));
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Wyvernpoacher_Drachlox");
	SetServerVariable("[PH]Wyvernpoacher_Drachlox", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;